package net.styleru.ikomarov.socialapp.contracts.social_network;

/**
 * Created by i_komarov on 23.04.17.
 */

public enum SocialNetwork {
    FACEBOOK(0x0000000),
    VKONTAKTE(0x0000001),
    TWITTER_SUCK_COCKS(0x0000002),
    INSTAGRAM(0x0000003);

    private final int code;

    SocialNetwork(int code) {
        this.code = code;
    }
}
