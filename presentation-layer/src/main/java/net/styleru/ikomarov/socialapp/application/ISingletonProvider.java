package net.styleru.ikomarov.socialapp.application;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.ErrorTranslationModule;
import net.styleru.ikomarov.domain_layer.di.level_2.NetworkModule;
import net.styleru.ikomarov.domain_layer.di.level_3.SessionModule;

import io.reactivex.Scheduler;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface ISingletonProvider {

    @NonNull
    NetworkModule network();

    @NonNull
    SessionModule session();

    @NonNull
    ErrorTranslationModule errorTranslation();

    @NonNull
    Scheduler ioScheduler();

    @NonNull
    Scheduler mainThreadScheduler();
}
