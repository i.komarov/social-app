package net.styleru.ikomarov.socialapp.presenter.base;

import net.styleru.ikomarov.socialapp.view.base.IView;

/**
 * Created by i_komarov on 22.04.17.
 */


public interface IPresenter<V extends IView> {

    void attachView(V view);

    void onResume();

    void onPause();

    void detachView();
}
