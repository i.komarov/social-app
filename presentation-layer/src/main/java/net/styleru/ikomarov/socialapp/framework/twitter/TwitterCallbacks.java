package net.styleru.ikomarov.socialapp.framework.twitter;

import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface TwitterCallbacks {

    void onTwitterSessionCreated(TwitterSession session);

    void onTwitterSessionFailed(TwitterException exception);
}
