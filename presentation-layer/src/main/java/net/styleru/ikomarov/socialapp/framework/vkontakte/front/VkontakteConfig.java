package net.styleru.ikomarov.socialapp.framework.vkontakte.front;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.socialapp.BuildConfig;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.04.17.
 */

public class VkontakteConfig {

    @NonNull
    public static final String FIELD_ACCESS_TOKEN = "access_token";

    @NonNull
    public static final String FIELD_EXPIRES_IN = "expires_in";

    @NonNull
    public static final String FIELD_USER_ID = "user_id";

    @NonNull
    public static final String BASE_URL = "https://oauth.vk.com";

    @NonNull
    public static final String ENDPOINT_ACCESS_TOKEN = "/access_token";

    @NonNull
    public static final String QUERY_CODE = "code";

    @NonNull
    public static final String QUERY_GRANT_TYPE = "grant_type";

    @NonNull
    public static final String QUERY_STATE = "state";

    @NonNull
    public static final String QUERY_ACCESS_TOKEN = "access_token";

    @NonNull
    public static final String QUERY_ERROR = "error";

    @NonNull
    public static final String ENDPOINT_AUTHORIZE = "/authorize";

    @NonNull
    public static final String QUERY_CLIENT_ID = "client_id";

    @NonNull
    public static final String QUERY_CLIENT_SECRET = "client_secret";

    @NonNull
    public static final String QUERY_REDIRECT_URI = "redirect_uri";

    @NonNull
    public static final String QUERY_DISPLAY = "display";

    @NonNull
    public static final String QUERY_SCOPE = "scope";

    @NonNull
    public static final String QUERY_VERSION = "v";

    @NonNull
    public static final String QUERY_REVOKE = "revoke";

    @NonNull
    public static final List<String> scopes = Arrays.asList(
            "friends",
            "notes",
            "wall",
            "offline",
            "groups"
    );

    private VkontakteConfig() {

    }

    public static VkontakteConfig getDefault() {
        return new VkontakteConfig();
    }

    public String getAuthUrl() {
        return BASE_URL + ENDPOINT_AUTHORIZE + "?" +
                QUERY_CLIENT_ID + "=" + BuildConfig.VKONTAKTE_ID + "&" +
                QUERY_REDIRECT_URI + "=" + BuildConfig.VKONTAKTE_REDIRECT_URI + "&" +
                QUERY_DISPLAY + "=" + "mobile" + "&" +
                QUERY_SCOPE + "=" + Observable.fromIterable(scopes).reduce("", (scope, element) -> scope += element + ",").map(scope -> scope.substring(0, scope.length() - 1)).blockingGet() + "&" +
                QUERY_VERSION + "=" + "5.63" + "&" +
                QUERY_STATE + "=" + UUID.randomUUID().toString() + "&" +
                QUERY_REVOKE + "=" + "1";
    }
}
