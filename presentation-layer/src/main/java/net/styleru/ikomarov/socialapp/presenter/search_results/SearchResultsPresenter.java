package net.styleru.ikomarov.socialapp.presenter.search_results;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;
import net.styleru.ikomarov.socialapp.view.search_result.ISearchResultsView;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultsPresenter extends Presenter<ISearchResultsView> {

    private SearchResultsPresenter() {

    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {

    }

    public static final class Factory implements RetainContainer.Factory<SearchResultsPresenter> {

        public Factory() {

        }

        @Override
        public SearchResultsPresenter create() {
            return new SearchResultsPresenter();
        }
    }
}
