package net.styleru.ikomarov.socialapp.contracts.facebook;

/**
 * Created by i_komarov on 22.04.17.
 */

public enum FBResponseType {
    CODE("code"),
    TOKEN("token"),
    CODE_TOKEN("code%20token"),
    GRANTED_SCOPES("granted_scopes");

    private final String type;

    FBResponseType(String type) {
        this.type = type;
    }

    public String getType() {
        return this.type;
    }
}
