package net.styleru.ikomarov.socialapp.application;

import android.app.Application;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.AppComponent;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.ErrorTranslationModule;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.NetworkModule;
import net.styleru.ikomarov.domain_layer.di.level_3.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.SessionModule;
import net.styleru.ikomarov.domain_layer.di.level_4.IClientComponent;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by i_komarov on 22.04.17.
 */

public class App extends Application implements ISingletonProvider {

    //THE ONE WOULD SUFFER
    //WHO DISRESPECT DI FRAMEWORKS
    //ENJOY THE HORROR

    private IAppComponent app;

    private IEnvironmentComponent environment;

    private ISessionComponent session;

    private IClientComponent client;

    private final Scheduler ioScheduler = Schedulers.io();

    private final Scheduler mainScheduler = AndroidSchedulers.mainThread();

    @Override
    public void onCreate() {
        super.onCreate();
        buildDependencyGraph();
    }

    @NonNull
    @Override
    public NetworkModule network() {
        return environment.network();
    }

    @NonNull
    @Override
    public SessionModule session() {
        return session.session();
    }

    @NonNull
    @Override
    public ErrorTranslationModule errorTranslation() {
        return environment.errorTranslation();
    }

    @NonNull
    @Override
    public Scheduler ioScheduler() {
        return ioScheduler;
    }

    @NonNull
    @Override
    public Scheduler mainThreadScheduler() {
        return mainScheduler;
    }

    private void buildDependencyGraph() {
        //top-level
        app = AppComponent.fromAppProcess(this);
        //second-level
        environment = app.plusEnvironmentComponent();
        //third-level
        session = environment.plusSessionComponent();
        //forth-level
        client = session.plusClientComponent();

    }
}
