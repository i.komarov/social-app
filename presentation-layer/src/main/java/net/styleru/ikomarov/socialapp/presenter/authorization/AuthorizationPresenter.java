package net.styleru.ikomarov.socialapp.presenter.authorization;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;
import net.styleru.ikomarov.socialapp.view.authorization.IAuthorizationView;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;

/**
 * Created by i_komarov on 22.04.17.
 */

public class AuthorizationPresenter extends Presenter<IAuthorizationView> {

    private AuthorizationPresenter() {

    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {

    }

    public static final class Factory implements RetainContainer.Factory<AuthorizationPresenter> {

        public Factory() {

        }

        @Override
        public AuthorizationPresenter create() {
            return new AuthorizationPresenter();
        }
    }
}
