package net.styleru.ikomarov.socialapp.view.drawer;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.view.search_result.SearchResultsFragment;
import net.styleru.ikomarov.socialapp.view.social_networks.SocialNetworksFragment;

public class DrawerActivity extends AppCompatActivity implements IDrawerView, NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout drawerLayout;

    private NavigationView navigationView;

    private Toolbar toolbar;

    private ActionBarDrawerToggle drawerToggle;

    @Override
    public void onBackPressed() {
        if(getFragmentManager().getBackStackEntryCount() > 1) {
            drawerToggle.setDrawerIndicatorEnabled(false);
        } else {
            drawerToggle.setDrawerIndicatorEnabled(true);
        }
        if(getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawer_activity);

        if(getFragmentManager().findFragmentById(R.id.drawer_activity_fragment_container) == null) {
            getFragmentManager().beginTransaction()
                    .replace(R.id.drawer_activity_fragment_container, SocialNetworksFragment.newInstance())
                    .commit();
        }

        bindUserInterface();
    }

    @Override
    public void onResume() {
        super.onResume();
        drawerToggle.setDrawerIndicatorEnabled(getFragmentManager().getBackStackEntryCount() == 0);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        final int id = item.getItemId();

        Fragment fragment = null;

        switch(id) {
            case R.id.menu_item_social_networks: {
                fragment = SocialNetworksFragment.newInstance();
                break;
            }
            case R.id.menu_item_search: {
                fragment = SearchResultsFragment.newInstance();
                break;
            }
        }

        if(fragment != null) {
            int depth = getFragmentManager().getBackStackEntryCount();
            while(depth-- > 0) {
                getFragmentManager().popBackStack();
            }
            getFragmentManager().beginTransaction().replace(R.id.drawer_activity_fragment_container, fragment).commit();

        }

        drawerLayout.closeDrawer(GravityCompat.START);

        return false;
    }

    private void bindUserInterface() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_activity_drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.drawer_activity_navigation_view);
        toolbar = (Toolbar) findViewById(R.id.activity_drawer_toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle("");
        toolbar.setTitle("");

        setupDrawer();
    }

    private void setupDrawer() {
        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.string.drawer_opened, R.string.drawer_closed);
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerToggle.syncState();

        drawerLayout.addDrawerListener(drawerToggle);

        toolbar.setNavigationOnClickListener((view) -> {
            if(drawerToggle.isDrawerIndicatorEnabled()) {
                drawerLayout.openDrawer(GravityCompat.START);
            } else {
                onBackPressed();
            }
        });

        navigationView.setNavigationItemSelectedListener(this);
    }
}
