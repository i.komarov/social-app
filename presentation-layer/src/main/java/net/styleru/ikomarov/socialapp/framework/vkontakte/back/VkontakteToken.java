package net.styleru.ikomarov.socialapp.framework.vkontakte.back;

import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteConfig;

/**
 * Created by i_komarov on 23.04.17.
 */

public class VkontakteToken {

    @SerializedName(VkontakteConfig.FIELD_ACCESS_TOKEN)
    private final String accessToken;

    @SerializedName(VkontakteConfig.FIELD_USER_ID)
    private final String userId;

    @SerializedName(VkontakteConfig.FIELD_EXPIRES_IN)
    private final int expiresIn;

    VkontakteToken(String accessToken, String userId, int expiresIn) {
        this.accessToken = accessToken;
        this.userId = userId;
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getUserId() {
        return userId;
    }

    public int getExpiresIn() {
        return expiresIn;
    }
}
