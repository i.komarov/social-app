package net.styleru.ikomarov.socialapp.view.search_result;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.socialapp.contracts.social_network.SocialNetwork;

import java.util.Map;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultViewObject {

    @NonNull
    private final String id;

    @NonNull
    private final String name;

    @NonNull
    private final String photoUrl;

    @NonNull
    private final Map<SocialNetwork, String> links;

    @NonNull
    private final String criteria;

    @NonNull
    private final String relation;

    public SearchResultViewObject(@NonNull String id, @NonNull String name, @NonNull String photoUrl, @NonNull Map<SocialNetwork, String> links, @NonNull String criteria, @NonNull String relation) {
        this.id = id;
        this.name = name;
        this.photoUrl = photoUrl;
        this.links = links;
        this.criteria = criteria;
        this.relation = relation;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getPhotoUrl() {
        return photoUrl;
    }

    @NonNull
    public Map<SocialNetwork, String> getLinks() {
        return links;
    }

    @NonNull
    public String getCriteria() {
        return criteria;
    }

    @NonNull
    public String getRelation() {
        return relation;
    }
}
