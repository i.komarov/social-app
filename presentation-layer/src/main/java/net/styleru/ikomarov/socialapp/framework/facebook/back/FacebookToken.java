package net.styleru.ikomarov.socialapp.framework.facebook.back;

import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookConfig;

/**
 * Created by i_komarov on 22.04.17.
 */

public class FacebookToken {

    @SerializedName(FacebookConfig.FIELD_ACCESS_TOKEN)
    private final String accessToken;

    @SerializedName(FacebookConfig.FIELD_TOKEN_TYPE)
    private final String tokenType;

    @SerializedName(FacebookConfig.FIELD_EXPIRES_IN)
    private final int expiresIn;

    FacebookToken(String accessToken, String tokenType, int expiresIn) {
        this.accessToken = accessToken;
        this.tokenType = tokenType;
        this.expiresIn = expiresIn;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public String getTokenType() {
        return tokenType;
    }

    public int getExpiresIn() {
        return expiresIn;
    }
}
