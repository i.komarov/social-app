package net.styleru.ikomarov.socialapp.view.base;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;

/**
 * Created by i_komarov on 22.04.17.
 */

public abstract class PresenterActivity<V extends IView, P extends Presenter<V>> extends AppCompatActivity implements IView {

    private static final String TAG = PresenterActivity.class.getSimpleName();

    private final Object lock = new Object();

    private volatile P presenter;

    private volatile boolean isViewAttached = false;

    private ActivityResultBundle resultBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resultBundle = new ActivityResultBundle();
        verifyContainerExists();
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter = getPresenter();
        if(!isViewAttached) {
            presenter.attachView(getView());
            isViewAttached = true;
        }
        ActivityResultBundle.Result result;
        while(null != (result = resultBundle.pollIfExists())) {
            handleActivityResult(result.getRequestCode(), result.getResultCode(), result.getData());
        }
        presenter.onResume();
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStop() {
        presenter.onPause();
        presenter.detachView();
        isViewAttached = false;
        presenter = null;
        super.onStop();
    }

    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.resultBundle.push(requestCode, resultCode, data);
    }

    protected abstract void handleActivityResult(int requestCode, int resultCode, Intent data);

    protected abstract RetainContainer.Factory<P> providePresenterFactory();

    protected abstract V getView();

    protected final P getPresenter() {
        P local = presenter;
        if (local == null) {
            synchronized (lock) {
                local = presenter;
                if (local == null) {
                    local = loadPresenter();
                    local.attachView(getView());
                    presenter = local;
                    isViewAttached = true;
                }
            }
        }

        return local;
    }

    private P loadPresenter() {
        return ((RetainContainer<P>) getFragmentManager().findFragmentByTag(TAG)).get();
    }

    private void verifyContainerExists() {
        RetainContainer<P> container = (RetainContainer<P>) getFragmentManager().findFragmentByTag(TAG);
        if(container == null) {
            synchronized (lock) {
                container = (RetainContainer<P>) getFragmentManager().findFragmentByTag(TAG);
                if(container == null) {
                    container = new RetainContainer<>();
                    container.setDataFactory(providePresenterFactory());
                    getFragmentManager().beginTransaction()
                            .add(container, TAG)
                            .commit();
                }
            }
        }
    }
}

