package net.styleru.ikomarov.socialapp.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.view.search_result.SearchResultViewObject;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private CircleImageView photoHolder;

    @NonNull
    private AppCompatTextView fullNameHolder;

    @NonNull
    private AppCompatImageButton optionsButton;

    @NonNull
    private AppCompatTextView contentTextHolder;

    @NonNull
    private AppCompatTextView relationHolder;

    private final int photoSize;

    public SearchResultViewHolder(@NonNull View itemView) {
        super(itemView);

        photoSize = itemView.getResources().getDimensionPixelSize(R.dimen.avatar_size);

        photoHolder = (CircleImageView) itemView.findViewById(R.id.list_item_search_result_author_avatar);
        fullNameHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_search_result_author_name);
        optionsButton = (AppCompatImageButton) itemView.findViewById(R.id.list_item_search_result_action_options);
        contentTextHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_search_result_expanded_criteria);
        relationHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_search_result_relations);
    }

    public void bind(RequestManager glide, SearchResultViewObject item) {
        fullNameHolder.setText(item.getName());
        contentTextHolder.setText(item.getCriteria());
        relationHolder.setText(item.getRelation());

        glide.load(item.getPhotoUrl())
                .override(photoSize, photoSize)
                .into(photoHolder);
    }
}
