package net.styleru.ikomarov.socialapp.framework.facebook.back;

import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface FacebookOAuth {

    @GET(FacebookConfig.ENDPOINT_TOKEN)
    Call<FacebookToken> trade(
            @Query(FacebookConfig.QUERY_CLIENT_ID) String clientId,
            @Query(FacebookConfig.QUERY_REDIRECT_URI) String redirectUri,
            @Query(FacebookConfig.QUERY_CLIENT_SECRET) String clientSecret,
            @Query(FacebookConfig.QUERY_CODE) String code
    );
}
