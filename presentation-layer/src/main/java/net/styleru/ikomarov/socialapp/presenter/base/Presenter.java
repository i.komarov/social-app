package net.styleru.ikomarov.socialapp.presenter.base;

import net.styleru.ikomarov.socialapp.view.base.IView;

/**
 * Created by i_komarov on 22.04.17.
 */

public abstract class Presenter<V extends IView> implements IPresenter<V> {

    private final Object lock = new Object();

    private V view;

    @Override
    public final void attachView(V view) {
        synchronized (lock) {
            this.view = view;
        }
        onViewAttached();
    }

    @Override
    public final void detachView() {
        onViewDetached();
        synchronized (lock) {
            this.view = null;
        }
    }

    protected final V getView() {
        synchronized (lock) {
            return this.view;
        }
    }

    protected abstract void onViewAttached();

    protected abstract void onViewDetached();

    protected final void postAction(ViewAction<V> action) {
        synchronized (lock) {
            if (view != null) {
                action.apply(view);
            }
        }
    }

    protected interface ViewAction<V extends IView> {

        void apply(V view);
    }
}

