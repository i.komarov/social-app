package net.styleru.ikomarov.socialapp.framework.facebook.front;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface FacebookCallbacks {

    void onFacebookAccessGranted(String code);

    void onFacebookTokenGranted(String token, String type, int expiration);

    void onFacebookTokenDenied(Throwable e);

    void onFacebookAccessDenied(String reason, String error, String description);
}
