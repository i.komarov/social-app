package net.styleru.ikomarov.socialapp.view.search_result;

import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.adapter.SearchResultsAdapter;
import net.styleru.ikomarov.socialapp.contracts.social_network.SocialNetwork;
import net.styleru.ikomarov.socialapp.presenter.search_results.SearchResultsPresenter;
import net.styleru.ikomarov.socialapp.view.base.PresenterFragment;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;

import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultsFragment extends PresenterFragment<ISearchResultsView, SearchResultsPresenter> implements ISearchResultsView {

    private RecyclerView list;

    private SearchResultsAdapter adapter;

    public static Fragment newInstance() {
        return new SearchResultsFragment();
    }

    @Override
    protected void bindUserInterface(View view) {
        adapter = new SearchResultsAdapter(Glide.with(getActivity()));
        list = (RecyclerView) view.findViewById(R.id.fragment_search_list);
        list.setLayoutManager(new LinearLayoutManager(getActivity()));
        list.setAdapter(adapter);

        adapter.add(Arrays.asList(
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей"),
                new SearchResultViewObject("1", "Igor Komarov", "https://www.w3schools.com/w3css/img_avatar3.png", new HashMap<SocialNetwork, String>(), "Subscribed to pancake restaurant yesterday. Has a lot of mutual friends with you", "Ваш друг, есть 10 общих друзей")
        ));
    }

    @Override
    protected RetainContainer.Factory<SearchResultsPresenter> providePresenterFactory() {
        return new SearchResultsPresenter.Factory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_search;
    }

    @Override
    public ISearchResultsView getViewInterface() {
        return this;
    }
}
