package net.styleru.ikomarov.socialapp.presenter.drawer;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;
import net.styleru.ikomarov.socialapp.view.drawer.IDrawerView;

/**
 * Created by i_komarov on 22.04.17.
 */

public class DrawerPresenter extends Presenter<IDrawerView> {
    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {

    }
}
