package net.styleru.ikomarov.socialapp.view.social_networks;

import android.app.Fragment;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.adapter.SocialNetworksAdapter;
import net.styleru.ikomarov.socialapp.contracts.social_network.SocialNetwork;
import net.styleru.ikomarov.socialapp.presenter.social_networks.SocialNetworksPresenter;
import net.styleru.ikomarov.socialapp.view.base.PresenterFragment;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;
import net.styleru.ikomarov.socialapp.view_holder.SocialNetworkViewHolder;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksFragment extends PresenterFragment<ISocialNetworksView, SocialNetworksPresenter> implements ISocialNetworksView, SocialNetworkViewHolder.InteractionCallbacks {

    private RecyclerView list;

    private SocialNetworksAdapter adapter;

    public static Fragment newInstance() {
        return new SocialNetworksFragment();
    }

    @Override
    protected void bindUserInterface(View view) {
        adapter = new SocialNetworksAdapter(Glide.with(getActivity()));
        list = (RecyclerView) view.findViewById(R.id.fragment_social_networks_list_list);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        list.setAdapter(adapter);
        adapter.setCallbacks(this);
    }

    @Override
    protected RetainContainer.Factory<SocialNetworksPresenter> providePresenterFactory() {
        return new SocialNetworksPresenter.Factory();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_social_networks_list;
    }

    @Override
    public ISocialNetworksView getViewInterface() {
        return this;
    }

    @Override
    public void toggleSwitches(SocialNetwork type, boolean isAttaching) {
        Log.d("SocialNetworksFragment", "social network: " + type.name() + ", is attaching: " + isAttaching);
    }
}
