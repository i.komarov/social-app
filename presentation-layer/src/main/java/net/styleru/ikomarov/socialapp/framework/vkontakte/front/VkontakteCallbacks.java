package net.styleru.ikomarov.socialapp.framework.vkontakte.front;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface VkontakteCallbacks {

    void onVkontakteCodeGranted(String code, String state);

    void onVkontakteCodeFailed(String message);

    void onVkontakteTokenGranted(String token, String userId, int expiresIn);

    void onVkontakteTokenFailed(Throwable e);
}
