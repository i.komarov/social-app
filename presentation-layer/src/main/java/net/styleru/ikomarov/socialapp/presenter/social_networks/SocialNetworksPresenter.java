package net.styleru.ikomarov.socialapp.presenter.social_networks;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;
import net.styleru.ikomarov.socialapp.view.social_networks.ISocialNetworksView;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksPresenter extends Presenter<ISocialNetworksView> {

    private SocialNetworksPresenter() {

    }

    @Override
    protected void onViewAttached() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onPause() {

    }

    @Override
    protected void onViewDetached() {

    }

    public static final class Factory implements RetainContainer.Factory<SocialNetworksPresenter> {

        public Factory() {

        }

        @Override
        public SocialNetworksPresenter create() {
            return new SocialNetworksPresenter();
        }
    }
}
