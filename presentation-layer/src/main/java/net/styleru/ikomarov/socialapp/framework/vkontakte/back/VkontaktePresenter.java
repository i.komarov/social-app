package net.styleru.ikomarov.socialapp.framework.vkontakte.back;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import net.styleru.ikomarov.socialapp.BuildConfig;
import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteCallbacks;
import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteConfig;

import java.net.HttpURLConnection;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 23.04.17.
 */

public class VkontaktePresenter {

    @NonNull
    private final VkontakteConfig config;

    @NonNull
    private final Retrofit retrofit;

    @NonNull
    private final VkontakteOAuth service;

    public VkontaktePresenter(@NonNull VkontakteConfig config) {
        this.config = config;

        this.retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .baseUrl(VkontakteConfig.BASE_URL)
                .client(new OkHttpClient.Builder().addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)).build())
                .build();

        this.service = retrofit.create(VkontakteOAuth.class);
    }

    public void trade(String code, VkontakteCallbacks callbacks) {
        service.trade(BuildConfig.VKONTAKTE_ID, BuildConfig.VKONTAKTE_PRIVATE_KEY, BuildConfig.VKONTAKTE_REDIRECT_URI, code)
                .enqueue(new Callback<VkontakteToken>() {
                    @Override
                    public void onResponse(Call<VkontakteToken> call, Response<VkontakteToken> response) {
                        VkontakteToken token = response.body();
                        if(response.code() == HttpURLConnection.HTTP_OK && token != null) {
                            callbacks.onVkontakteTokenGranted(token.getAccessToken(), token.getUserId(), token.getExpiresIn());
                        }
                    }

                    @Override
                    public void onFailure(Call<VkontakteToken> call, Throwable t) {
                        callbacks.onVkontakteTokenFailed(t);
                    }
                });
    }
}
