package net.styleru.ikomarov.socialapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.contracts.social_network.SocialNetwork;
import net.styleru.ikomarov.socialapp.view_holder.SocialNetworkViewHolder;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksAdapter extends RecyclerView.Adapter<SocialNetworkViewHolder> {

    @NonNull
    private final List<SocialNetwork> items = Arrays.asList(SocialNetwork.values());

    @NonNull
    private final Map<SocialNetwork, Boolean> statusMap = new HashMap<>();

    @NonNull
    private final RequestManager glide;

    @NonNull
    private SocialNetworkViewHolder.InteractionCallbacks callbacks = (type, isAttaching) -> {

    };

    public SocialNetworksAdapter(RequestManager glide) {
        this.glide = glide;
        for(SocialNetwork item : items) {
            statusMap.put(item, false);
        }
    }

    @Override
    public SocialNetworkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SocialNetworkViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_social_network, parent, false));
    }

    @Override
    public void onBindViewHolder(final SocialNetworkViewHolder holder, final int position) {
        holder.bind(glide, callbacks, items.get(position), statusMap.get(items.get(position)));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void setCallbacks(@NonNull SocialNetworkViewHolder.InteractionCallbacks callbacks) {
        this.callbacks = callbacks;
    }
}
