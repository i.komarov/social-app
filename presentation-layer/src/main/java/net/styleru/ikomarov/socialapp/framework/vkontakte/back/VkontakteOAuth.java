package net.styleru.ikomarov.socialapp.framework.vkontakte.back;

import com.google.gson.JsonObject;

import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteConfig;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface VkontakteOAuth {

    @GET(VkontakteConfig.ENDPOINT_ACCESS_TOKEN)
    Call<VkontakteToken> trade(
            @Query(VkontakteConfig.QUERY_CLIENT_ID) String clientId,
            @Query(VkontakteConfig.QUERY_CLIENT_SECRET) String clientSecret,
            @Query(VkontakteConfig.QUERY_REDIRECT_URI) String redirectUri,
            @Query(VkontakteConfig.QUERY_CODE) String code
    );
}
