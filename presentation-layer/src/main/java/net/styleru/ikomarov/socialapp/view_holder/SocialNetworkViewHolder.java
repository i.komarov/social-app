package net.styleru.ikomarov.socialapp.view_holder;

import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.contracts.social_network.SocialNetwork;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkViewHolder extends RecyclerView.ViewHolder {

    @NonNull
    private final AppCompatImageView logoHolder;

    @NonNull
    private final AppCompatTextView titleHolder;

    @NonNull
    private final AppCompatTextView statusHolder;

    @NonNull
    private final AppCompatButton toggleStatusButton;

    @NonNull
    private final String[] enableDisableToggle;

    public SocialNetworkViewHolder(View itemView) {
        super(itemView);

        this.logoHolder = (AppCompatImageView) itemView.findViewById(R.id.list_item_social_network_logo);
        this.titleHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_social_network_title);
        this.statusHolder = (AppCompatTextView) itemView.findViewById(R.id.list_item_social_network_status);
        this.toggleStatusButton = (AppCompatButton) itemView.findViewById(R.id.list_item_social_network_action_toggle);

        this.enableDisableToggle = itemView.getResources().getStringArray(R.array.enable_disable_toggle);
    }

    public void bind(RequestManager glide, InteractionCallbacks callbacks, SocialNetwork type, boolean isActivated) {
        if(logoHolder.getDrawable() != null) {
            Glide.clear(logoHolder);
        }

        switch(type) {
            case FACEBOOK: {
                glide.load(R.drawable.ic_facebook).into(logoHolder);
                titleHolder.setText(R.string.title_facebook);
                break;
            }
            case VKONTAKTE: {
                glide.load(R.drawable.ic_vkontakte).into(logoHolder);
                titleHolder.setText(R.string.title_vkontakte);
                break;
            }
            case INSTAGRAM: {
                glide.load(R.drawable.ic_instagram).into(logoHolder);
                titleHolder.setText(R.string.title_instagram);
                break;
            }
            case TWITTER_SUCK_COCKS: {
                //TWITTER SUCK BIG BLACK COCKS
                glide.load(R.drawable.ic_twitter).into(logoHolder);
                titleHolder.setText(R.string.title_twitter_suck_cocks);
                break;
            }
        }

        this.toggleStatusButton.setText(enableDisableToggle[isActivated ? 0 : 1]);
    }

    public interface InteractionCallbacks {

        void toggleSwitches(SocialNetwork type, boolean isAttaching);
    }
}
