package net.styleru.ikomarov.socialapp.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.RequestManager;

import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.view.search_result.SearchResultViewObject;
import net.styleru.ikomarov.socialapp.view_holder.SearchResultViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultsAdapter extends RecyclerView.Adapter<SearchResultViewHolder> {

    @NonNull
    private final RequestManager glide;

    @NonNull
    private final List<SearchResultViewObject> items;

    public SearchResultsAdapter(@NonNull RequestManager glide) {
        this.glide = glide;
        this.items = new ArrayList<>();
    }

    @Override
    public SearchResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new SearchResultViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_search_result, parent, false));
    }

    @Override
    public void onBindViewHolder(SearchResultViewHolder holder, int position) {
        holder.bind(glide, items.get(position));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public void add(List<SearchResultViewObject> items) {
        final int startPosition = this.items.size();
        this.items.addAll(items);
        notifyItemRangeInserted(startPosition, items.size());
    }
}
