package net.styleru.ikomarov.socialapp.view.authorization;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.WebView;

import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import net.styleru.ikomarov.socialapp.BuildConfig;
import net.styleru.ikomarov.socialapp.R;
import net.styleru.ikomarov.socialapp.contracts.authorization.AuthorizationType;
import net.styleru.ikomarov.socialapp.framework.facebook.back.FacebookPresenter;
import net.styleru.ikomarov.socialapp.framework.twitter.TwitterCallbacks;
import net.styleru.ikomarov.socialapp.framework.twitter.TwitterPresenter;
import net.styleru.ikomarov.socialapp.framework.vkontakte.back.VkontaktePresenter;
import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteCallbacks;
import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteConfig;
import net.styleru.ikomarov.socialapp.framework.vkontakte.front.VkontakteWebViewClient;
import net.styleru.ikomarov.socialapp.presenter.authorization.AuthorizationPresenter;
import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookCallbacks;
import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookConfig;
import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookWebViewClient;
import net.styleru.ikomarov.socialapp.view.base.PresenterActivity;
import net.styleru.ikomarov.socialapp.view.base.RetainContainer;

import io.fabric.sdk.android.Fabric;

/**
 * Created by i_komarov on 22.04.17.
 */

public class AuthorizationActivity extends PresenterActivity<IAuthorizationView, AuthorizationPresenter> implements IAuthorizationView, FacebookCallbacks, TwitterCallbacks, VkontakteCallbacks {

    @NonNull
    private static final String PREFIX = AuthorizationActivity.class.getCanonicalName() + ".";

    @NonNull
    public static final String KEY_TOKEN = PREFIX + "TOKEN";

    private WebView webView;

    private ActivityState state;

    private TwitterPresenter twitterPresenter;

    private FacebookPresenter facebookPresenter;

    private VkontaktePresenter vkontaktePresenter;

    public static Intent newIntent(Activity activity, AuthorizationType type) {
        Intent intent = new Intent(activity, AuthorizationActivity.class);
        intent.putExtra(ActivityState.KEY_AUTHORIZATION_TYPE, (Parcelable) type);
        return intent;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        state = new ActivityState(getIntent(), savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(BuildConfig.TWITTER_APP_ID, BuildConfig.TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        twitterPresenter = new TwitterPresenter(this, new TwitterAuthClient());
        facebookPresenter = new FacebookPresenter(FacebookConfig.getDefault());
        vkontaktePresenter = new VkontaktePresenter(VkontakteConfig.getDefault());
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authorization_activity);
        bindUserInterface();
    }

    @Override
    protected void handleActivityResult(int requestCode, int resultCode, Intent data) {
        twitterPresenter.onResult(requestCode, resultCode, data);
    }

    @Override
    public void onVkontakteCodeGranted(String code, String state) {
        Log.d("AuthorizationActivity#onVkontakteCodeGranted", "code: " + code + ", state: " + state);
        vkontaktePresenter.trade(code, this);
    }

    @Override
    public void onVkontakteCodeFailed(String message) {
        Log.d("AuthorizationActivity#onVkontakteCodeFailed", "error: " + message);
    }

    @Override
    public void onVkontakteTokenGranted(String token, String userId, int expiresIn) {
        Log.d("AuthorizationActivity#onVkontakteTokenGranted", "token: " + token + ", userId: " + userId + ", expiresIn: " + expiresIn);
    }

    @Override
    public void onVkontakteTokenFailed(Throwable e) {
        Log.d("AuthorizationActivity#onVkontakteTokenFailed", "error: " + e.getMessage());
        e.printStackTrace();
    }

    @Override
    public void onFacebookAccessGranted(String code) {
        Log.d("AuthorizationActivity#onFacebookAccessGranted", "fb code: " + code);
        facebookPresenter.trade(code, this);
    }

    @Override
    public void onFacebookTokenGranted(String token, String type, int expiration) {
        Log.d("AuthorizationActivity#onFacebookAccessGranted", "fb token: " + token + ", type: " + type + ", expiration: " + expiration);
    }

    @Override
    public void onFacebookTokenDenied(Throwable e) {
        Log.d("AuthorizationActivity#onFacebookTokenDenied", e.getMessage());
        e.printStackTrace();
    }

    @Override
    public void onFacebookAccessDenied(String reason, String error, String description) {
        Log.d("AuthorizationActivity#onFacebookAccessDenied", "fb reason: " + reason + ", error: " + error + ", description: " + description);
    }

    @Override
    public void onTwitterSessionCreated(TwitterSession session) {
        Log.d("AuthorizationActivity#onTwitterSessionCreated", "session: " + session.getUserName() + ", " + session.getAuthToken());
    }

    @Override
    public void onTwitterSessionFailed(TwitterException exception) {
        Log.d("AuthorizationActivity#onTwitterSessionCreated", "exception: " + exception.getMessage());
        exception.printStackTrace();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void bindUserInterface() {
        webView = (WebView) findViewById(R.id.authorization_activity_web_view);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);

        switch(state.type) {
            case FACEBOOK: {
                FacebookConfig config;
                webView.setWebViewClient(new FacebookWebViewClient(config = FacebookConfig.getDefault(), this));
                webView.loadUrl(config.getAuthUrl());
                break;
            }
            case VKONTAKTE: {
                webView.setWebViewClient(new VkontakteWebViewClient(this));
                webView.loadUrl(VkontakteConfig.getDefault().getAuthUrl());
                break;
            }
            case TWITTER: {
                twitterPresenter.authorize(this);
                break;
            }
            case INSTAGRAM: {

                break;
            }
        }
    }

    @Override
    protected RetainContainer.Factory<AuthorizationPresenter> providePresenterFactory() {
        return new AuthorizationPresenter.Factory();
    }

    @Override
    protected IAuthorizationView getView() {
        return null;
    }

    private static final class ActivityState {

        @NonNull
        private static final String PREFIX = ActivityState.class.getCanonicalName();

        @NonNull
        private static final String KEY_AUTHORIZATION_TYPE = PREFIX + "AUTHORIZATION_TYPE";

        @NonNull
        private AuthorizationType type;

        private ActivityState(Intent intent, Bundle savedInstanceState) {
            Bundle extras;
            if(savedInstanceState != null) {
                if (savedInstanceState.containsKey(KEY_AUTHORIZATION_TYPE)) {
                    type = savedInstanceState.getParcelable(KEY_AUTHORIZATION_TYPE);
                }
            } else if(intent != null && (extras = intent.getExtras()) != null) {
                if(extras.containsKey(KEY_AUTHORIZATION_TYPE)) {
                    type = extras.getParcelable(KEY_AUTHORIZATION_TYPE);
                }
            }
        }
    }
}
