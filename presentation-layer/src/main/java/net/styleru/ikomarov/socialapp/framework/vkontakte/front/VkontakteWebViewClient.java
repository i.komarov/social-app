package net.styleru.ikomarov.socialapp.framework.vkontakte.front;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by i_komarov on 22.04.17.
 */

public class VkontakteWebViewClient extends WebViewClient {

    @NonNull
    private final VkontakteCallbacks callbacks;

    public VkontakteWebViewClient(@NonNull VkontakteCallbacks callbacks) {
        this.callbacks = callbacks;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if(url.contains("#")) {
            url = url.replaceFirst("#", "?");
        }

        Uri uri = Uri.parse(url);

        String code = null;
        String error = null;

        if(null != (code = uri.getQueryParameter(VkontakteConfig.QUERY_CODE))) {
            String state = uri.getQueryParameter(VkontakteConfig.QUERY_STATE);
            callbacks.onVkontakteCodeGranted(code, state);
        } else if(null != (error = uri.getQueryParameter(VkontakteConfig.QUERY_ERROR))) {
            callbacks.onVkontakteCodeFailed(uri.getQueryParameter(VkontakteConfig.QUERY_ERROR));
        }

        if(code != null || error != null) {
            view.loadUrl("about:blank");
        }
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        url = url.replaceFirst("#", "?");
        Log.d("VkontakteWebViewClient", "url: " + url);
        return super.shouldOverrideUrlLoading(view, url);
    }
}
