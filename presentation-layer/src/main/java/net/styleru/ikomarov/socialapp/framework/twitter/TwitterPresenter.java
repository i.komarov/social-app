package net.styleru.ikomarov.socialapp.framework.twitter;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;

import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

/**
 * Created by i_komarov on 22.04.17.
 */

public class TwitterPresenter {

    @NonNull
    private final Activity activity;

    @NonNull
    private final TwitterAuthClient client;

    public TwitterPresenter(@NonNull Activity activity, @NonNull TwitterAuthClient client) {
        this.activity = activity;
        this.client = client;
    }

    public void authorize(final TwitterCallbacks callback) {
        client.authorize(activity, new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                callback.onTwitterSessionCreated(result.data);
            }

            @Override
            public void failure(TwitterException exception) {
                callback.onTwitterSessionFailed(exception);
            }
        });
    }

    public void onResult(int requestCode, int resultCode, Intent data) {
        client.onActivityResult(requestCode, resultCode, data);
    }
}
