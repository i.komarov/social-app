package net.styleru.ikomarov.socialapp.contracts.authorization;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by i_komarov on 22.04.17.
 */

public enum AuthorizationType implements Parcelable {
    FACEBOOK(0x0000000),
    VKONTAKTE(0x0000001),
    TWITTER(0x0000002),
    INSTAGRAM(0x0000003);

    private final int code;

    AuthorizationType(int code) {
        this.code = code;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(code);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<AuthorizationType> CREATOR = new Creator<AuthorizationType>() {
        @Override
        public AuthorizationType createFromParcel(Parcel in) {
            int code = in.readInt();
            if(code == FACEBOOK.code) {
                return FACEBOOK;
            } else if(code == VKONTAKTE.code) {
                return VKONTAKTE;
            } else if(code == TWITTER.code) {
                return TWITTER;
            } else if(code == INSTAGRAM.code) {
                return INSTAGRAM;
            } else return null;
        }

        @Override
        public AuthorizationType[] newArray(int size) {
            return new AuthorizationType[size];
        }
    };
}
