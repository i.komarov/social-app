package net.styleru.ikomarov.socialapp.framework.facebook.back;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;

import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookCallbacks;
import net.styleru.ikomarov.socialapp.framework.facebook.front.FacebookConfig;

import java.net.HttpURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 22.04.17.
 */

public class FacebookPresenter {

    @NonNull
    private final FacebookConfig config;

    @NonNull
    private final Retrofit retrofit;

    @NonNull
    private final FacebookOAuth service;

    public FacebookPresenter(@NonNull FacebookConfig config) {
        this.config = config;

        this.retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .baseUrl(FacebookConfig.BASE_URL)
                .build();

        this.service = retrofit.create(FacebookOAuth.class);
    }

    public void trade(String code, FacebookCallbacks callbacks) {
        service.trade(config.getClientId(), config.getRedirectUri(), config.getSecret(), code)
                .enqueue(new Callback<FacebookToken>() {
                    @Override
                    public void onResponse(Call<FacebookToken> call, Response<FacebookToken> response) {
                        FacebookToken token = response.body();
                        if(response.code() == HttpURLConnection.HTTP_OK && token != null) {
                            callbacks.onFacebookTokenGranted(token.getAccessToken(), token.getTokenType(), token.getExpiresIn());
                        } else {
                            Log.d("FacebookPresenter", "response code: " + response.code());
                        }
                    }

                    @Override
                    public void onFailure(Call<FacebookToken> call, Throwable t) {
                        callbacks.onFacebookTokenDenied(t);
                    }
                });
    }
}
