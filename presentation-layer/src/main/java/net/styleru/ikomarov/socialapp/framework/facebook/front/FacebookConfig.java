package net.styleru.ikomarov.socialapp.framework.facebook.front;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.socialapp.BuildConfig;
import net.styleru.ikomarov.socialapp.contracts.facebook.FBResponseType;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.04.17.
 */

public class FacebookConfig {

    @NonNull
    public static final String BASE_URL = "https://graph.facebook.com";

    @NonNull
    public static final String ENDPOINT_TOKEN = "/oauth/access_token";

    @NonNull
    public static final String FIELD_CODE = "code";

    @NonNull
    public static final String FIELD_ERROR = "error";

    @NonNull
    public static final String FIELD_ERROR_REASON = "error_reason";

    @NonNull
    public static final String FIELD_ERROR_DESCRIPTION = "error_description";


    @NonNull
    public static final String FIELD_ACCESS_TOKEN = "access_token";

    @NonNull
    public static final String FIELD_TOKEN_TYPE = "token_type";

    @NonNull
    public static final String FIELD_EXPIRES_IN = "expires_in";


    @NonNull
    public static final String BASE_URL_OAUTH2 = "https://www.facebook.com/dialog/oauth?";

    @NonNull
    public static final String QUERY_CLIENT_ID = "client_id";

    @NonNull
    public static final String QUERY_GRANT_TYPE = "grant_type";

    @NonNull
    public static final String QUERY_RESPONSE_TYPE = "response_type";

    @NonNull
    public static final String QUERY_SCOPE = "scope";

    @NonNull
    public static final String QUERY_REDIRECT_URI = "redirect_uri";

    @NonNull
    public static final String QUERY_CLIENT_SECRET = "client_secret";

    @NonNull
    public static final String QUERY_EXCHANGE_TOKEN = "fb_exchange_token";

    @NonNull
    public static final String QUERY_CODE = "code";

    @NonNull
    public static final String QUERY_STATE = "state";


    @NonNull
    private final String clientId;

    @NonNull
    private final String responseType;

    @NonNull
    private final String scope;

    @NonNull
    private final String redirectUri;

    @NonNull
    private final String state;

    private static final List<String> scopes = Arrays.asList(
            "public_profile",
            "user_friends",
            "email"
    );

    public static FacebookConfig getDefault() {
        return new FacebookConfig(
                String.valueOf(BuildConfig.FACEBOOK_APP_ID),
                FBResponseType.CODE.getType(),
                Observable.fromIterable(scopes).reduce("", (scope, element) -> scope += element + ",").map(scope -> scope.substring(0, scope.length() - 1)).blockingGet(),
                BuildConfig.FACEBOOK_REDIRECT_URI,
                UUID.randomUUID().toString()
        );
    }

    private FacebookConfig(@NonNull String clientId, @NonNull String responseType, @NonNull String scope, @NonNull String redirectUri, @NonNull String state) {
        this.clientId = clientId;

        this.responseType = responseType;
        this.scope = scope;
        this.redirectUri = redirectUri;
        this.state = state;
    }

    @NonNull
    public String getSecret() {
        return BuildConfig.FACEBOOK_SECRET;
    }

    @NonNull
    public String getClientId() {
        return this.clientId;
    }

    @NonNull
    public String getRedirectUri() {
        return this.redirectUri;
    }

    @NonNull
    public String getAuthUrl() {
        String url = BASE_URL_OAUTH2;
        url += QUERY_CLIENT_ID + "=" + clientId + "&";
        url += QUERY_RESPONSE_TYPE + "=" + responseType + "&";
        url += QUERY_SCOPE + "=" + scope + "&";
        url += QUERY_REDIRECT_URI + "=" + redirectUri + "&";
        url += QUERY_STATE + "=" + state;
        return url;
    }
}
