package net.styleru.ikomarov.socialapp.framework.facebook.front;

import android.graphics.Bitmap;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by i_komarov on 22.04.17.
 */

public class FacebookWebViewClient extends WebViewClient {

    @NonNull
    private final FacebookConfig config;

    @NonNull
    private final FacebookCallbacks callbacks;

    public FacebookWebViewClient(FacebookConfig config, @NonNull FacebookCallbacks callbacks) {
        this.config = config;
        this.callbacks = callbacks;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        Uri uri = Uri.parse(url);

        String code = null;
        String error = null;

        if(null != (code = uri.getQueryParameter(FacebookConfig.FIELD_CODE))) {
            callbacks.onFacebookAccessGranted(code);
        } else if(null != (error = uri.getQueryParameter(FacebookConfig.FIELD_ERROR))) {
            callbacks.onFacebookAccessDenied(
                    uri.getQueryParameter(FacebookConfig.FIELD_ERROR_REASON),
                    error,
                    uri.getQueryParameter(FacebookConfig.FIELD_ERROR_DESCRIPTION)
            );
        }

        if(code != null || error != null) {
            view.loadUrl("about:blank");
        }
    }
}
