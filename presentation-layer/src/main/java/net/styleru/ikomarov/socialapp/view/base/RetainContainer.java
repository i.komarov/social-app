package net.styleru.ikomarov.socialapp.view.base;

import android.app.Fragment;
import android.os.Bundle;

/**
 * Created by i_komarov on 22.04.17.
 */

public class RetainContainer<T> extends Fragment {

    private final Object lock = new Object();

    private volatile T data;

    private Factory<T> factory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //TODO: make this retain somehow
        //this.setRetainInstance(true);
    }

    public void setDataFactory(Factory<T> factory) {
        this.factory = factory;
    }

    public T get() {
        T localData = data;
        if(localData == null) {
            synchronized (lock) {
                localData = data;
                if(localData == null) {
                    if(factory != null) {
                        localData = data = factory.create();
                    }
                }
            }
        }

        return localData;
    }

    public interface Factory<T> {

        T create();
    }
}

