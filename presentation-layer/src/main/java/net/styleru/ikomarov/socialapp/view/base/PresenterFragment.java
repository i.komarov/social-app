package net.styleru.ikomarov.socialapp.view.base;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import net.styleru.ikomarov.socialapp.presenter.base.Presenter;

/**
 * Created by i_komarov on 23.04.17.
 */

public abstract class PresenterFragment<V extends IView, P extends Presenter<V>> extends Fragment implements IView {

    private static final String TAG = PresenterFragment.class.getSimpleName();

    private P presenter;

    private ActivityResultBundle resultBundle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.resultBundle = new ActivityResultBundle();
        if(getChildFragmentManager().findFragmentByTag(TAG) == null) {
            RetainContainer<P> fragment = new RetainContainer<>();
            fragment.setDataFactory(providePresenterFactory());
            getChildFragmentManager().beginTransaction()
                    .add(fragment, TAG)
                    .commit();
        }
    }

    @Override
    public final View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        return inflater.inflate(getLayoutRes(), parent, false);
    }

    @Override
    public final void onViewCreated(View view, Bundle savedInstanceState) {
        bindUserInterface(view);
    }

    protected final P getPresenter() {
        return this.presenter;
    }

    @Override
    public void onStart() {
        super.onStart();
        presenter = ((RetainContainer<P>) getChildFragmentManager().findFragmentByTag(TAG)).get();
        ActivityResultBundle.Result result;
        while(null != (result = resultBundle.pollIfExists())) {
            handleActivityResult(result.getRequestCode(), result.getResultCode(), result.getData());
        }
        presenter.onResume();
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.attachView(getViewInterface());
    }

    @Override
    public final void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        this.resultBundle.push(requestCode, resultCode, data);
    }

    protected void handleActivityResult(int requestCode, int resultCode, Intent data) {
        //Do nothing by default
    }

    @Override
    public void onPause() {
        presenter.detachView();
        super.onPause();
    }

    @Override
    public void onStop() {
        presenter.onPause();
        presenter = null;
        super.onStop();
    }

    protected abstract void bindUserInterface(View view);

    protected abstract RetainContainer.Factory<P> providePresenterFactory();

    @LayoutRes
    protected abstract int getLayoutRes();

    public abstract V getViewInterface();
}

