package net.styleru.ikomarov.data_layer.entities.search;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.search.SearchRequestEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchRequestEntity {

    @NonNull
    @SerializedName(FIELD_QUERY)
    private final String query;

    private SearchRequestEntity(@NonNull String query) {
        this.query = query;
    }

    @NonNull
    public String getQuery() {
        return query;
    }

    public static final class Builder {

        @NonNull
        private final String query;

        public Builder(@NonNull String query) {
            this.query = query;
        }

        public SearchRequestEntity create() {
            return new SearchRequestEntity(query);
        }
    }
}
