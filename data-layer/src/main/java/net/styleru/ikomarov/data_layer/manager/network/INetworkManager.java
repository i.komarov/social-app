package net.styleru.ikomarov.data_layer.manager.network;

import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface INetworkManager {

    Observable<NetworkStatus> getNetworkStatusChangeEventsObservable();

    Observable<NetworkStatus> getCurrentNetworkStatusObservable();

    NetworkStatus getCurrentNetworkStatus();
}
