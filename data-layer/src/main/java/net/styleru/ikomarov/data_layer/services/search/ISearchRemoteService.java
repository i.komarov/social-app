package net.styleru.ikomarov.data_layer.services.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface ISearchRemoteService {

    @NonNull
    String FIELD_QUERY = "request";


    /**
     * successful response is indicated with {@link java.net.HttpURLConnection#HTTP_OK} or {@link java.net.HttpURLConnection#HTTP_PARTIAL}
     * */
    @GET(Endpoints.ENDPOINT_SEARCH)
    Observable<Response<SearchResponse>> search(
        @Query(FIELD_QUERY) String query
    );
}
