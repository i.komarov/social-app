package net.styleru.ikomarov.data_layer.entities.session;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.session.SessionTokenEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionTokenEntity {

    @NonNull
    @SerializedName(FIELD_TOKEN)
    private final String token;

    private SessionTokenEntity(@NonNull String token) {
        this.token = token;
    }

    @NonNull
    public String getToken() {
        return token;
    }
}
