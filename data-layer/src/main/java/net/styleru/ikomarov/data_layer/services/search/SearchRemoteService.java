package net.styleru.ikomarov.data_layer.services.search;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchRemoteService extends NetworkService<ISearchRemoteService> implements ISearchRemoteService {

    private SearchRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.BASE_URL, client);
    }

    @Override
    public Observable<Response<SearchResponse>> search(@NonNull String query) {
        return getService().search(query)
                .compose(handleError());
    }

    @Override
    protected ISearchRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ISearchRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        public ISearchRemoteService create() {
            return new SearchRemoteService(client);
        }
    }
}
