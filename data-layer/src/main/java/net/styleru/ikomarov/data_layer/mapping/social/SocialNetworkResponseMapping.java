package net.styleru.ikomarov.data_layer.mapping.social;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkResponse;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkResponseMapping implements IRemoteMapping<SocialNetworkResponse> {

    @NonNull
    public static final String FIELD_NETWORK_NAME = "networkName";

    @NonNull
    public static final String FIELD_USER_ID = "userIdInNetwork";

    @NonNull
    private final Gson gson;

    public SocialNetworkResponseMapping() {
        this.gson = new Gson();
    }

    @Override
    public SocialNetworkResponse fromJson(JsonObject json) {
        return gson.fromJson(json, SocialNetworkResponse.class);
    }

    @Override
    public List<SocialNetworkResponse> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SocialNetworkResponse>>(){}.getType());
    }

    @Override
    public String toJson(SocialNetworkResponse entity) {
        return gson.toJson(entity, SocialNetworkResponse.class);
    }

    @Override
    public String toJsonArray(List<SocialNetworkResponse> entities) {
        return gson.toJson(entities, new TypeToken<List<SocialNetworkResponse>>(){}.getType());
    }
}
