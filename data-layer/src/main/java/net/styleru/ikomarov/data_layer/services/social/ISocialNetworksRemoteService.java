package net.styleru.ikomarov.data_layer.services.social;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;
import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface ISocialNetworksRemoteService {

    /**
     * successful response is indicated with {@link java.net.HttpURLConnection#HTTP_CREATED}
     * */
    @POST(Endpoints.ENDPOINT_SOCIAL_NETWORKS)
    Observable<Response<Object>> associate(
            @Body SocialNetworkEntity body
    );

    /**
     * successful response is indicated with {@link java.net.HttpURLConnection#HTTP_OK}
     * */
    @GET(Endpoints.ENDPOINT_SOCIAL_NETWORKS)
    Observable<Response<List<SocialNetworkEntity>>> list(

    );
}
