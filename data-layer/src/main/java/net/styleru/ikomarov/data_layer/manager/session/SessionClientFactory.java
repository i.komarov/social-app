package net.styleru.ikomarov.data_layer.manager.session;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.common.Endpoints;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 23.04.17.
 */

public final class SessionClientFactory {

    private static final int READ_TIMEOUT = 60;
    private static final int WRITE_TIMEOUT = 15;
    private static final int CONNECT_TIMEOUT = 15;

    private static final Object lock = new Object();

    private volatile Retrofit retrofit;

    public SessionClientFactory() {

    }

    public Retrofit get() {
        Retrofit localInstance = retrofit;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = retrofit;
                if(localInstance == null) {
                    localInstance = retrofit = buildRetrofit();
                }
            }
        }

        return localInstance;
    }

    @NonNull
    private Retrofit buildRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Endpoints.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(buildClient())
                .build();
    }

    @NonNull
    private OkHttpClient buildClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();
    }
}
