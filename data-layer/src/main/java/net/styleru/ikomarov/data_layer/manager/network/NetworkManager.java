package net.styleru.ikomarov.data_layer.manager.network;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;

import java.util.concurrent.atomic.AtomicBoolean;

import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;

/**
 * Created by i_komarov on 22.04.17.
 */

public class NetworkManager implements INetworkManager {

    private Context context;

    private IntentFilter filter;

    public NetworkManager(Context context) {
        this.context = context.getApplicationContext();
        this.filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
    }

    @Override
    public Observable<NetworkStatus> getNetworkStatusChangeEventsObservable() {
        return createNetworkStatusChangeEventsObservable();
    }

    @Override
    public Observable<NetworkStatus> getCurrentNetworkStatusObservable() {
        return Observable.just(getStatus(context));
    }

    @Override
    public NetworkStatus getCurrentNetworkStatus() {
        return getStatus(context);
    }

    private Observable<NetworkStatus> createNetworkStatusChangeEventsObservable() {
        return Observable.create(emitter -> {
            final BroadcastReceiver networkInfoReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context1, Intent intent) {
                    if(!emitter.isDisposed()) {
                        try {
                            emitter.onNext(getStatus(context1));
                        } catch(Exception e) {
                            emitter.onError(e);
                        }
                    }
                }
            };

            emitter.setDisposable(new Disposable() {

                private AtomicBoolean isDisposed = new AtomicBoolean(false);

                @Override
                public void dispose() {
                    isDisposed.set(true);
                    context.unregisterReceiver(networkInfoReceiver);
                }

                @Override
                public boolean isDisposed() {
                    return isDisposed.get();
                }
            });

            context.registerReceiver(networkInfoReceiver, filter);
        });
    }

    @NonNull
    private NetworkStatus getStatus(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivityManager != null) {
            NetworkInfo info = connectivityManager.getActiveNetworkInfo();
            if(info != null) {
                if(info.getType() == ConnectivityManager.TYPE_WIFI) {
                    return NetworkStatus.CONNECTED_WIFI;
                } else if(info.getType() == ConnectivityManager.TYPE_MOBILE) {
                    return NetworkStatus.CONNECTED_MOBILE;
                } else {
                    return NetworkStatus.DISCONNECTED;
                }
            } else {
                return NetworkStatus.DISCONNECTED;
            }
        } else {
            return NetworkStatus.DISCONNECTED;
        }
    }
}
