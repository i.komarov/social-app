package net.styleru.ikomarov.data_layer.mapping.search;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.search.SearchRequestEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchRequestEntityMapping implements IRemoteMapping<SearchRequestEntity> {

    @NonNull
    public static final String FIELD_QUERY = "request";

    @NonNull
    private final Gson gson;

    public SearchRequestEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public SearchRequestEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SearchRequestEntity.class);
    }

    @Override
    public List<SearchRequestEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SearchRequestEntity>>(){}.getType());
    }

    @Override
    public String toJson(SearchRequestEntity entity) {
        return gson.toJson(entity, SearchRequestEntity.class);
    }

    @Override
    public String toJsonArray(List<SearchRequestEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SearchRequestEntity>>(){}.getType());
    }
}
