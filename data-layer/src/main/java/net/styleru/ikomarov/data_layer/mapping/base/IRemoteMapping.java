package net.styleru.ikomarov.data_layer.mapping.base;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.util.List;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface IRemoteMapping<T> {

    T fromJson(JsonObject json);

    List<T> fromJsonArray(JsonArray json);

    String toJson(T entity);

    String toJsonArray(List<T> entities);
}
