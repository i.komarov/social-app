package net.styleru.ikomarov.data_layer.manager.session;

import android.content.SharedPreferences;

import net.styleru.ikomarov.data_layer.contracts.errors.HttpErrorContract;
import net.styleru.ikomarov.data_layer.contracts.errors.InternalErrorContract;
import net.styleru.ikomarov.data_layer.contracts.network.NetworkStatus;
import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.entities.session.SessionRequestEntity;
import net.styleru.ikomarov.data_layer.entities.session.SessionTokenEntity;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;
import net.styleru.ikomarov.data_layer.manager.network.INetworkManager;
import net.styleru.ikomarov.data_layer.services.session.ISessionRemoteService;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionManager implements ISessionManager {

    private static final class Config {

        static final String PREFIX = SessionManager.class.getCanonicalName() + ".";

        static final String KEY_ACCESS_TOKEN = PREFIX + "REFRESH_TOKEN";

        static final String KEY_USERNAME = PREFIX + "USERNAME";

        static final String KEY_PASSWORD = PREFIX + "PASSWORD";
    }

    private final Object preferencesLock = new Object();

    private final Object lock = new Object();

    private final INetworkManager networkManager;

    private final SessionClientFactory clientFactory;

    private final ISessionRemoteService service;

    private final SharedPreferences preferences;

    private volatile String accessToken;

    private volatile boolean isSessionActive = false;
    private volatile boolean isAuthenticating = false;

    private Map<String, IAuthenticationCallbacks> sessionCallbacks;

    public SessionManager(SharedPreferences prefereces, INetworkManager networkManager) {
        this.networkManager = networkManager;
        this.preferences = prefereces;
        this.clientFactory = new SessionClientFactory();
        this.service = clientFactory.get().create(ISessionRemoteService.class);
        this.sessionCallbacks = new HashMap<>();
    }

    @Override
    public Observable<SessionStatus> getCurrentSessionStatus() {
        return Observable.just(isAuthenticating ? SessionStatus.AUTHENTICATING :
                isSessionActive ? SessionStatus.ACTIVE : SessionStatus.INACTIVE);
    }

    @Override
    public Observable<String> getSessionToken() {
        if(accessToken != null || preferences.contains(Config.KEY_ACCESS_TOKEN)) {
            return Observable.just(accessToken != null ? accessToken : preferences.getString(Config.KEY_ACCESS_TOKEN, null));
        } else {
            return Observable.error(new ExceptionBundle(Reason.SESSION_EXPIRED));
        }
    }

    @Override
    public String blockingGetSessionToken() throws ExceptionBundle {
        return getSessionToken().blockingFirst();
    }

    @Override
    public void registerCallback(String key, IAuthenticationCallbacks callbacks) {
        synchronized (lock) {
            this.sessionCallbacks.put(key, callbacks);
        }
    }

    @Override
    public void unregisterCallback(String key) {
        synchronized (lock) {
            this.sessionCallbacks.remove(key);
        }
    }

    @Override
    public void deauthenticate() {
        publishDeauthentication();
        isSessionActive = false;
        accessToken = null;
    }

    @Override
    public void authenticate(String username, String password) throws ExceptionBundle {
        if(!isSessionActive && !isAuthenticating) {
            if (networkManager.getCurrentNetworkStatus() != NetworkStatus.DISCONNECTED) {
                isAuthenticating = true;
                service.authenticate(new SessionRequestEntity.Builder(username, password).create())
                        .enqueue(new Callback<SessionTokenEntity>() {
                            @Override
                            public void onResponse(Call<SessionTokenEntity> call, Response<SessionTokenEntity> response) {
                                //very rough assertion that any of 200-299 codes fit our needs
                                if(response.isSuccessful()) {
                                    SessionTokenEntity token = response.body();
                                    accessToken = token.getToken();
                                    publishAuthenticationSuccess();
                                    postPreferencesAction((preferences1) -> preferences1.edit()
                                            .putString(Config.KEY_ACCESS_TOKEN, accessToken)
                                            .putString(Config.KEY_USERNAME, username)
                                            .putString(Config.KEY_PASSWORD, password)
                                            .commit());

                                } else {
                                    ExceptionBundle error = new ExceptionBundle(Reason.HTTP_BAD_CODE);
                                    error.addIntExtra(HttpErrorContract.KEY_CODE, response.code());
                                    publishAuthenticationFailure(error);
                                }
                            }

                            @Override
                            public void onFailure(Call<SessionTokenEntity> call, Throwable thr) {
                                ExceptionBundle wrappedException;
                                if (thr instanceof ExceptionBundle) {
                                    wrappedException = (ExceptionBundle) thr;
                                } else {
                                    wrappedException = new ExceptionBundle(Reason.INTERNAL_DATA);
                                    wrappedException.addThrowableExtra(InternalErrorContract.KEY_THROWABLE, thr);
                                    wrappedException.addStringExtra(InternalErrorContract.KEY_MESSAGE, thr.getMessage());
                                }

                                publishAuthenticationFailure(wrappedException);
                            }
                        });
            } else {
                throw new ExceptionBundle(Reason.NETWORK_UNAVAILABLE);
            }
        }
    }

    private void publishAuthenticationFailure(ExceptionBundle error) {
        isAuthenticating = false;
        isSessionActive = false;
        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onAuthenticationFailed(error);
            }
        }
    }

    private void publishAuthenticationSuccess() {
        isAuthenticating = false;
        isSessionActive = true;
        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onAuthenticated();
            }
        }
    }

    private void publishDeauthentication() {
        postPreferencesAction((preferences) -> {
            preferences.edit().remove(Config.KEY_ACCESS_TOKEN)
                    .remove(Config.KEY_PASSWORD)
                    .remove(Config.KEY_USERNAME)
                    .commit();
        });

        isAuthenticating = false;
        isSessionActive = false;

        synchronized (lock) {
            for(IAuthenticationCallbacks callback : sessionCallbacks.values()) {
                callback.onDeauthenticated();
            }
        }
    }

    private void postPreferencesAction(PreferencesAction action) {
        synchronized (preferencesLock) {
            action.apply(preferences);
        }
    }

    private interface PreferencesAction {

        void apply(SharedPreferences preferences);
    }
}
