package net.styleru.ikomarov.data_layer.services.users;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.user.UserEntity;
import net.styleru.ikomarov.data_layer.entities.user.UserRequestEntity;
import net.styleru.ikomarov.data_layer.manager.network.NetworkManager;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UsersRemoteService extends NetworkService<IUsersRemoteService> implements IUsersRemoteService {

    private UsersRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.BASE_URL, client);
    }

    @Override
    public Observable<Response<UserEntity>> register(@Body UserRequestEntity body) {
        return getService().register(body)
                .compose(handleError());
    }

    @Override
    protected IUsersRemoteService createService(Retrofit retrofit) {
        return retrofit.create(IUsersRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        public IUsersRemoteService create() {
            return new UsersRemoteService(client);
        }
    }
}
