package net.styleru.ikomarov.data_layer.manager.session;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface IAuthenticationCallbacks {

    void onAuthenticated();

    void onDeauthenticated();

    void onAuthenticationFailed(ExceptionBundle error);
}
