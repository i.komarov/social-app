package net.styleru.ikomarov.data_layer.entities.search;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.search.SearchContainerEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResponse {

    @NonNull
    @SerializedName(FIELD_COUNT)
    private final Long count;

    @NonNull
    @SerializedName(FIELD_DATA)
    private final List<SearchResultEntity> results;

    private SearchResponse(@NonNull Long count, @NonNull List<SearchResultEntity> results) {
        this.count = count;
        this.results = results;
    }

    @NonNull
    public Long getCount() {
        return count;
    }

    @NonNull
    public List<SearchResultEntity> getResults() {
        return results;
    }
}
