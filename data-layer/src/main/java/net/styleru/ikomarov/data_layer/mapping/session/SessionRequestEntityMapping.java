package net.styleru.ikomarov.data_layer.mapping.session;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.session.SessionRequestEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionRequestEntityMapping implements IRemoteMapping<SessionRequestEntity> {

    @NonNull
    public static final String FIELD_USERNAME = "username";

    @NonNull
    public static final String FIELD_PASSWORD = "password";

    @NonNull
    private final Gson gson;

    public SessionRequestEntityMapping(@NonNull Gson gson) {
        this.gson = gson;
    }

    @Override
    public SessionRequestEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SessionRequestEntity.class);
    }

    @Override
    public List<SessionRequestEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SessionRequestEntity>>(){}.getType());
    }

    @Override
    public String toJson(SessionRequestEntity entity) {
        return gson.toJson(entity, SessionRequestEntity.class);
    }

    @Override
    public String toJsonArray(List<SessionRequestEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SessionRequestEntity>>(){}.getType());
    }
}
