package net.styleru.ikomarov.data_layer.manager.session.interceptor;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;

/**
 * Created by i_komarov on 23.04.17.
 */

public class ClientSessionInterceptor extends SessionInterceptor {

    @NonNull
    private final ISessionManager manager;

    public ClientSessionInterceptor(@NonNull ISessionManager manager) {
        this.manager = manager;
    }

    @Override
    protected String getToken() throws ExceptionBundle {
        return manager.blockingGetSessionToken();
    }
}
