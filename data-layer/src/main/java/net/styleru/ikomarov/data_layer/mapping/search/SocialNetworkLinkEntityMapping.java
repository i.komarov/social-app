package net.styleru.ikomarov.data_layer.mapping.search;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.search.SocialNetworkLinkEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkLinkEntityMapping implements IRemoteMapping<SocialNetworkLinkEntity> {

    @NonNull
    public static final String FIELD_NETWORK_NAME = "name";

    @NonNull
    public static final String FIELD_MESSAGE_LINK = "messageLink";

    @NonNull
    private final Gson gson;

    public SocialNetworkLinkEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public SocialNetworkLinkEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SocialNetworkLinkEntity.class);
    }

    @Override
    public List<SocialNetworkLinkEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SocialNetworkLinkEntity>>(){}.getType());
    }

    @Override
    public String toJson(SocialNetworkLinkEntity entity) {
        return gson.toJson(entity, SocialNetworkLinkEntity.class);
    }

    @Override
    public String toJsonArray(List<SocialNetworkLinkEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SocialNetworkLinkEntity>>(){}.getType());
    }
}
