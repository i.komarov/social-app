package net.styleru.ikomarov.data_layer.mapping.user;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.user.UserRequestEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserRequestEntityMapping implements IRemoteMapping<UserRequestEntity> {

    @NonNull
    public static final String FIELD_USERNAME = "username";

    @NonNull
    public static final String FIELD_PASSWORD = "password";

    @NonNull
    public static final String FIELD_EMAIL = "email";

    private final Gson gson;

    public UserRequestEntityMapping(Gson gson) {
        this.gson = gson;
    }

    @Override
    public UserRequestEntity fromJson(JsonObject json) {
        return gson.fromJson(json, UserRequestEntity.class);
    }

    @Override
    public List<UserRequestEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<UserRequestEntity>>(){}.getType());
    }

    @Override
    public String toJson(UserRequestEntity entity) {
        return gson.toJson(entity, UserRequestEntity.class);
    }

    @Override
    public String toJsonArray(List<UserRequestEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<UserRequestEntity>>(){}.getType());
    }
}
