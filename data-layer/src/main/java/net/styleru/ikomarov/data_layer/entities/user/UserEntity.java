package net.styleru.ikomarov.data_layer.entities.user;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.user.UserEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserEntity {

    @SerializedName(FIELD_USERNAME)
    private final String username;

    @SerializedName(FIELD_PASSWORD)
    private final String password;

    @SerializedName(FIELD_EMAIL)
    private final String email;

    private UserEntity(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }
}
