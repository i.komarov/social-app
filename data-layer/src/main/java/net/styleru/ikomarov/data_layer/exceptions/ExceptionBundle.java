package net.styleru.ikomarov.data_layer.exceptions;

import android.os.Bundle;
import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.04.17.
 */

public class ExceptionBundle extends Exception {

    @NonNull
    private final Reason reason;

    @NonNull
    private final Bundle extras;


    public ExceptionBundle(@NonNull Reason reason) {
        super();
        this.reason = reason;
        this.extras = new Bundle();
    }

    public ExceptionBundle(@NonNull Reason reason,
                           @NonNull Bundle extras) {
        super();
        this.reason = reason;
        this.extras = extras;
    }

    public void addStringExtra(String key, String value) {
        this.extras.putString(key, value);
    }

    public String getStringExtra(String key) {
        return this.extras.getString(key);
    }

    public void addIntExtra(String key, Integer value) {
        this.extras.putInt(key, value);
    }

    public Integer getIntExtra(String key) {
        return this.getIntExtra(key);
    }

    public void addThrowableExtra(String key, Throwable value) {
        this.extras.putSerializable(key, value);
    }

    public Throwable getThrowableExtra(String key) {
        return (Throwable) this.extras.getSerializable(key);
    }
}
