package net.styleru.ikomarov.data_layer.exceptions;

/**
 * Created by i_komarov on 22.04.17.
 */

public enum Reason {
    INTERNAL_DATA(00000000),
    INTERNAL_DOMAIN(00000001),
    INTERNAL_PRESENTATION(00000002),

    NETWORK_UNAVAILABLE(00000010),
    NETWORK_SSL_HANDSHAKE_FAILED(00000011),

    SESSION_EXPIRED(00000100),

    HTTP_BAD_CODE(00001000),


    ;
    private final int code;

    Reason(int code) {
        this.code = code;
    }

    public boolean isInternalError() {
        return 0 <= code && code <= 00000010;
    }
}

