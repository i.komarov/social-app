package net.styleru.ikomarov.data_layer.mapping.social;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkEntityMapping implements IRemoteMapping<SocialNetworkEntity> {

    @NonNull
    public static final String FIELD_NETWORK_NAME = "networkName";

    @NonNull
    public static final String FIELD_USER_ID = "userIdInNetwork";

    @NonNull
    public static final String FIELD_TOKEN = "socialNetworkToken";

    @NonNull
    private final Gson gson;

    public SocialNetworkEntityMapping(@NonNull Gson gson) {
        this.gson = gson;
    }

    @Override
    public SocialNetworkEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SocialNetworkEntity.class);
    }

    @Override
    public List<SocialNetworkEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SocialNetworkEntity>>(){}.getType());
    }

    @Override
    public String toJson(SocialNetworkEntity entity) {
        return gson.toJson(entity, SocialNetworkEntity.class);
    }

    @Override
    public String toJsonArray(List<SocialNetworkEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SocialNetworkEntity>>(){}.getType());
    }
}
