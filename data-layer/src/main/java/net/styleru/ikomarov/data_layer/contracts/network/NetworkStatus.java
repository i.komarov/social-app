package net.styleru.ikomarov.data_layer.contracts.network;

import android.util.SparseArray;

/**
 * Created by i_komarov on 22.04.17.
 */

public enum NetworkStatus {
    CONNECTED_MOBILE (0x0000000),
    CONNECTED_WIFI   (0x0000001),
    DISCONNECTED     (0x0000002);

    private static final SparseArray<NetworkStatus> statusMap;

    static {
        statusMap = new SparseArray<>(NetworkStatus.values().length);

        for(NetworkStatus val : NetworkStatus.values()) {
            statusMap.put(val.getCode(), val);
        }
    }

    private final int code;

    public static NetworkStatus forValue(int code) {
        if(statusMap.get(code) != null) {
            return statusMap.get(code);
        }

        return null;
    }

    NetworkStatus(int code) {
        this.code = code;
    }

    public int getCode() {
        return this.code;
    }
}
