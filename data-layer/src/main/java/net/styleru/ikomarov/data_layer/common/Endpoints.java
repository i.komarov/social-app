package net.styleru.ikomarov.data_layer.common;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.04.17.
 */

public class Endpoints {

    @NonNull
    public static final String BASE_URL = "https://socialapp.com";

    @NonNull
    public static final String ENDPOINT_AUTHENTICATE = "/authenticate";

    @NonNull
    public static final String ENDPOINT_REGISTRATION = "/registration";

    @NonNull
    public static final String ENDPOINT_SOCIAL_NETWORKS = "/integrations";

    @NonNull
    public static final String ENDPOINT_SEARCH = "/search";
}
