package net.styleru.ikomarov.data_layer.contracts.errors;

/**
 * Created by i_komarov on 22.04.17.
 */

public class InternalErrorContract {

    private static final String PREFIX = InternalErrorContract.class.getCanonicalName() + ".";

    public static final String KEY_THROWABLE = PREFIX + "THROWABLE";

    public static final String KEY_MESSAGE = PREFIX + "MESSAGE";
}
