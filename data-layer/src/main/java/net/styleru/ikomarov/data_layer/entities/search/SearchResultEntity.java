package net.styleru.ikomarov.data_layer.entities.search;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.search.SearchResultEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultEntity {

    @NonNull
    @SerializedName(FIELD_ID)
    private final String id;

    @NonNull
    @SerializedName(FIELD_NAME)
    private final String name;

    @NonNull
    @SerializedName(FIELD_PHOTO_URL)
    private final String photoUrl;

    @NonNull
    @SerializedName(FIELD_SOCIAL_NETWORKS)
    private final List<SocialNetworkLinkEntity> links;

    @NonNull
    @SerializedName(FIELD_CRITEREA)
    private final String criteria;

    @NonNull
    @SerializedName(FIELD_EXTENDED_CRITERIA)
    private final String extendedCriteria;

    @NonNull
    @SerializedName(FIELD_MATCH_SCORE)
    private final String matchScore;

    @NonNull
    @SerializedName(FIELD_RELATION)
    private final String relation;

    private SearchResultEntity(@NonNull String id,
                              @NonNull String name,
                              @NonNull String photoUrl,
                              @NonNull List<SocialNetworkLinkEntity> links,
                              @NonNull String criteria,
                              @NonNull String extendedCriteria,
                              @NonNull String matchScore,
                              @NonNull String relation) {

        this.id = id;
        this.name = name;
        this.photoUrl = photoUrl;
        this.links = links;
        this.criteria = criteria;
        this.extendedCriteria = extendedCriteria;
        this.matchScore = matchScore;
        this.relation = relation;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getPhotoUrl() {
        return photoUrl;
    }

    @NonNull
    public List<SocialNetworkLinkEntity> getLinks() {
        return links;
    }

    @NonNull
    public String getCriteria() {
        return criteria;
    }

    @NonNull
    public String getExtendedCriteria() {
        return extendedCriteria;
    }

    @NonNull
    public String getMatchScore() {
        return matchScore;
    }

    @NonNull
    public String getRelation() {
        return relation;
    }
}
