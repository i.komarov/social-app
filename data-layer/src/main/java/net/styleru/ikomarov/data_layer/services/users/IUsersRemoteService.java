package net.styleru.ikomarov.data_layer.services.users;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.user.UserEntity;
import net.styleru.ikomarov.data_layer.entities.user.UserRequestEntity;

import io.reactivex.Observable;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface IUsersRemoteService {

    /**
     * successful request is indicated with {@link java.net.HttpURLConnection#HTTP_CREATED}
     * */
    @POST(Endpoints.ENDPOINT_REGISTRATION)
    Observable<Response<UserEntity>> register(
            @Body UserRequestEntity body
    );
}
