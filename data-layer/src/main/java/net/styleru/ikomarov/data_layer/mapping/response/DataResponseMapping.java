package net.styleru.ikomarov.data_layer.mapping.response;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.base.DataResponse;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 22.04.17.
 */

public class DataResponseMapping<T> implements IRemoteMapping<DataResponse<T>> {

    @NonNull
    public static final String FIELD_BODY = "body";

    @NonNull
    public static final String FIELD_ERROR = "error";

    @NonNull
    private final Gson gson;

    public DataResponseMapping() {
        this.gson = new Gson();
    }

    @Override
    public DataResponse<T> fromJson(JsonObject json) {
        return gson.fromJson(json, new TypeToken<DataResponse<T>>(){}.getType());
    }

    @Override
    public List<DataResponse<T>> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<DataResponse<T>>>(){}.getType());
    }

    @Override
    public String toJson(DataResponse<T> entity) {
        return gson.toJson(entity, new TypeToken<DataResponse<T>>(){}.getType());
    }

    @Override
    public String toJsonArray(List<DataResponse<T>> entities) {
        return gson.toJson(entities, new TypeToken<List<DataResponse<T>>>(){}.getType());
    }
}
