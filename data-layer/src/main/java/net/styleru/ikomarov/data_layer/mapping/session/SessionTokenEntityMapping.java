package net.styleru.ikomarov.data_layer.mapping.session;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.session.SessionTokenEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionTokenEntityMapping implements IRemoteMapping<SessionTokenEntity> {

    @NonNull
    public static final String FIELD_TOKEN = "token";

    private final Gson gson;

    public SessionTokenEntityMapping(Gson gson) {
        this.gson = gson;
    }

    @Override
    public SessionTokenEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SessionTokenEntity.class);
    }

    @Override
    public List<SessionTokenEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SessionTokenEntity>>(){}.getType());
    }

    @Override
    public String toJson(SessionTokenEntity entity) {
        return gson.toJson(entity, SessionTokenEntity.class);
    }

    @Override
    public String toJsonArray(List<SessionTokenEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SessionTokenEntity>>(){}.getType());
    }
}
