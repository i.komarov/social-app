package net.styleru.ikomarov.data_layer.contracts.errors;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.04.17.
 */

public class HttpErrorContract {

    private static final String PREFIX = HttpErrorContract.class.getCanonicalName() + ".";

    @NonNull
    public static final String KEY_CODE = PREFIX + "CODE";

    @NonNull
    public static final String KEY_MESSAGE = PREFIX + "MESSAGE";
}
