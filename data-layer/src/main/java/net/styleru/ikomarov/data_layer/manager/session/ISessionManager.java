package net.styleru.ikomarov.data_layer.manager.session;

import net.styleru.ikomarov.data_layer.contracts.session.SessionStatus;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface ISessionManager {

    Observable<SessionStatus> getCurrentSessionStatus();

    Observable<String> getSessionToken();

    String blockingGetSessionToken() throws ExceptionBundle;

    void registerCallback(String key, IAuthenticationCallbacks callbacks);

    void unregisterCallback(String key);

    void deauthenticate();

    void authenticate(String username, String password) throws ExceptionBundle;
}

