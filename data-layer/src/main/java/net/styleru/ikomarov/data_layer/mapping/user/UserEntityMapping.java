package net.styleru.ikomarov.data_layer.mapping.user;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.user.UserEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserEntityMapping implements IRemoteMapping<UserEntity> {

    @NonNull
    public static final String FIELD_USERNAME = "username";

    @NonNull
    public static final String FIELD_PASSWORD = "password";

    @NonNull
    public static final String FIELD_EMAIL = "email";

    private final Gson gson;

    public UserEntityMapping(Gson gson) {
        this.gson = gson;
    }

    @Override
    public UserEntity fromJson(JsonObject json) {
        return gson.fromJson(json, UserEntity.class);
    }

    @Override
    public List<UserEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<UserEntity>>(){}.getType());
    }

    @Override
    public String toJson(UserEntity entity) {
        return gson.toJson(entity, UserEntity.class);
    }

    @Override
    public String toJsonArray(List<UserEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<UserEntity>>(){}.getType());
    }
}
