package net.styleru.ikomarov.data_layer.services.social;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;
import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkResponse;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import java.util.List;

import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.http.Body;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksRemoteService extends NetworkService<ISocialNetworksRemoteService> implements ISocialNetworksRemoteService {

    private SocialNetworksRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.BASE_URL, client);
    }

    @Override
    public Observable<Response<Object>> associate(@Body SocialNetworkEntity body) {
        return getService().associate(body)
                .compose(handleError());
    }

    @Override
    public Observable<Response<List<SocialNetworkEntity>>> list() {
        return getService().list()
                .compose(handleError());
    }

    @Override
    protected ISocialNetworksRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ISocialNetworksRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        public ISocialNetworksRemoteService create() {
            return new SocialNetworksRemoteService(client);
        }
    }
}
