package net.styleru.ikomarov.data_layer.entities.social;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.social.SocialNetworkResponseMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkResponse {

    @NonNull
    @SerializedName(FIELD_NETWORK_NAME)
    private final String name;

    @NonNull
    @SerializedName(FIELD_USER_ID)
    private final String userId;

    private SocialNetworkResponse(@NonNull String name, @NonNull String userId) {
        this.name = name;
        this.userId = userId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }
}
