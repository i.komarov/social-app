package net.styleru.ikomarov.data_layer.entities.base;

import com.google.gson.JsonElement;
import com.google.gson.annotations.SerializedName;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static net.styleru.ikomarov.data_layer.mapping.response.DataResponseMapping.*;

/**
 * Created by i_komarov on 22.04.17.
 */

public class DataResponse<T> extends DataModel<T> {

    @SerializedName(FIELD_BODY)
    private JsonElement bodyRaw;

    private T bodyActual;

    @SerializedName(FIELD_ERROR)
    private ExceptionBundle error;

    public static <T> DataResponse<T> unsuccessfulRequestResult(ExceptionBundle error) {
        DataResponse<T> response = new DataResponse<>();
        response.error = error;
        return response;
    }

    public static <T> DataResponse<T> successfulRequestResult(T data) {
        DataResponse<T> response = new DataResponse<>();
        response.bodyActual = data;
        return response;
    }

    public static <T> DataResponse<T> emptyRequestResult() {
        return new DataResponse<>();
    }

    private DataResponse() {

    }

    @Override
    public boolean containsError() {
        return error != null;
    }

    @Override
    public boolean containsData() {
        return bodyRaw != null || bodyActual != null;
    }

    @Override
    public List<T> getBody(IRemoteMapping<T> mapping) {
        if(bodyRaw != null) {
            if (bodyRaw.isJsonArray()) {
                return mapping.fromJsonArray(bodyRaw.getAsJsonArray());
            } else if (bodyRaw.isJsonObject()) {
                return Collections.singletonList(mapping.fromJson(bodyRaw.getAsJsonObject()));
            } else {
                return new ArrayList<>();
            }
        } else if(bodyActual != null) {
            return Collections.singletonList(bodyActual);
        } else {
            return new ArrayList<>(0);
        }
    }

    @Override
    public ExceptionBundle getError() {
        return error;
    }

    @Override
    public String toString() {
        return "DataResponse{" +
                "bodyRaw=" + bodyRaw +
                ", bodyActual=" + bodyActual +
                ", error=" + error +
                '}';
    }
}

