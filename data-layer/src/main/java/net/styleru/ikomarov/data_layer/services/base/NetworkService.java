package net.styleru.ikomarov.data_layer.services.base;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import net.styleru.ikomarov.data_layer.contracts.errors.InternalErrorContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;

import java.net.UnknownHostException;

import javax.net.ssl.SSLHandshakeException;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import okhttp3.OkHttpClient;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by i_komarov on 25.02.17.
 */

public abstract class NetworkService<S> {

    private Retrofit retrofit;

    private S service;

    public NetworkService(@NonNull String baseUrl, @NonNull OkHttpClient client) {
        this.retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .client(client)
                .build();

        bindService();
    }

    private void bindService() {
        this.service = createService(retrofit);
    }

    protected abstract S createService(Retrofit retrofit);

    protected abstract Gson getGson();

    protected S getService() {
        return this.service;
    }

    protected final <T> ObservableTransformer<T, T> handleError() {
        return upstream -> upstream.onErrorResumeNext(
                throwable -> {
                    return Observable.error(parseException(throwable));
                }
        );
    }

    private ExceptionBundle parseException(Throwable thr) {
        ExceptionBundle error;

        if(thr instanceof UnknownHostException) {
            error = new ExceptionBundle(Reason.NETWORK_UNAVAILABLE);
        } else if(thr instanceof SSLHandshakeException) {
            error = new ExceptionBundle(Reason.NETWORK_SSL_HANDSHAKE_FAILED);
        } else if(thr.getCause() != null && thr.getCause() instanceof ExceptionBundle) {
            error = (ExceptionBundle) thr.getCause();
        } else {
            error = new ExceptionBundle(Reason.INTERNAL_DATA);
            error.addThrowableExtra(InternalErrorContract.KEY_THROWABLE, thr);
            error.addStringExtra(InternalErrorContract.KEY_MESSAGE, thr.getMessage());
        }

        Log.e("NetworkService", "exception was raised", error);

        return error;
    }
}

