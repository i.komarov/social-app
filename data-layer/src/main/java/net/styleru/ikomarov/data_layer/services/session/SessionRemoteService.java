package net.styleru.ikomarov.data_layer.services.session;

import android.support.annotation.NonNull;

import com.google.gson.Gson;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.session.SessionRequestEntity;
import net.styleru.ikomarov.data_layer.entities.session.SessionTokenEntity;
import net.styleru.ikomarov.data_layer.services.base.NetworkService;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionRemoteService extends NetworkService<ISessionRemoteService> implements ISessionRemoteService {

    private SessionRemoteService(@NonNull OkHttpClient client) {
        super(Endpoints.BASE_URL, client);
    }

    @Override
    public Call<SessionTokenEntity> authenticate(@NonNull SessionRequestEntity request) {
        return getService().authenticate(request);
    }

    @Override
    protected ISessionRemoteService createService(Retrofit retrofit) {
        return retrofit.create(ISessionRemoteService.class);
    }

    @Override
    protected Gson getGson() {
        return new Gson();
    }

    public static final class Factory {

        @NonNull
        private final OkHttpClient client;

        public Factory(@NonNull OkHttpClient client) {
            this.client = client;
        }

        public ISessionRemoteService create() {
            return new SessionRemoteService(client);
        }
    }
}
