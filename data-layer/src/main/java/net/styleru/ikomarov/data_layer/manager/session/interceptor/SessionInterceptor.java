package net.styleru.ikomarov.data_layer.manager.session.interceptor;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;

import java.io.IOException;
import java.net.HttpURLConnection;

import okhttp3.Interceptor;
import okhttp3.Response;

/**
 * Created by i_komarov on 22.04.17.
 */

public abstract class SessionInterceptor implements Interceptor {

    private static final String HEADER_AUTHORIZATION = "Authorization";

    private static final String HEADER_BEARER = "Bearer ";

    @Override
    public final Response intercept(Chain chain) throws IOException {
        Response response = null;
        try {
            if (HttpURLConnection.HTTP_UNAUTHORIZED == (response = performRequest(chain, getToken())).code()) {
                response = performRequest(chain, getToken());
            }
        } catch(ExceptionBundle e) {
            throw new IOException(e);
        }

        return response;
    }

    private Response performRequest(Chain chain, String token) throws IOException {
        return chain.proceed(
                chain.request()
                        .newBuilder()
                        .header(HEADER_AUTHORIZATION, HEADER_BEARER + token)
                        .build()
        );
    }

    protected abstract String getToken() throws ExceptionBundle;
}

