package net.styleru.ikomarov.data_layer.mapping.search;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.search.SearchResultEntity;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultEntityMapping implements IRemoteMapping<SearchResultEntity> {

    @NonNull
    public static final String FIELD_ID = "id";

    @NonNull
    public static final String FIELD_NAME = "name";

    @NonNull
    public static final String FIELD_PHOTO_URL = "profile_pic";

    @NonNull
    public static final String FIELD_SOCIAL_NETWORKS = "social_networks";

    @NonNull
    public static final String FIELD_CRITEREA = "criteria";

    @NonNull
    public static final String FIELD_EXTENDED_CRITERIA = "extended_criteria";

    @NonNull
    public static final String FIELD_MATCH_SCORE = "match_score";

    @NonNull
    public static final String FIELD_RELATION = "relation";

    @NonNull
    private final Gson gson;

    public SearchResultEntityMapping(@NonNull Gson gson) {
        this.gson = gson;
    }

    @Override
    public SearchResultEntity fromJson(JsonObject json) {
        return gson.fromJson(json, SearchResultEntity.class);
    }

    @Override
    public List<SearchResultEntity> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SearchResultEntity>>(){}.getType());
    }

    @Override
    public String toJson(SearchResultEntity entity) {
        return gson.toJson(entity, SearchResultEntity.class);
    }

    @Override
    public String toJsonArray(List<SearchResultEntity> entities) {
        return gson.toJson(entities, new TypeToken<List<SearchResultEntity>>(){}.getType());
    }
}
