package net.styleru.ikomarov.data_layer.mapping.search;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchContainerEntityMapping implements IRemoteMapping<SearchResponse> {

    @NonNull
    public static final String FIELD_COUNT = "results_count";

    @NonNull
    public static final String FIELD_DATA = "results";

    @NonNull
    private final Gson gson;

    public SearchContainerEntityMapping() {
        this.gson = new Gson();
    }

    @Override
    public SearchResponse fromJson(JsonObject json) {
        return gson.fromJson(json, SearchResponse.class);
    }

    @Override
    public List<SearchResponse> fromJsonArray(JsonArray json) {
        return gson.fromJson(json, new TypeToken<List<SearchResponse>>(){}.getType());
    }

    @Override
    public String toJson(SearchResponse entity) {
        return gson.toJson(entity, SearchResponse.class);
    }

    @Override
    public String toJsonArray(List<SearchResponse> entities) {
        return gson.toJson(entities, new TypeToken<List<SearchResponse>>(){}.getType());
    }
}
