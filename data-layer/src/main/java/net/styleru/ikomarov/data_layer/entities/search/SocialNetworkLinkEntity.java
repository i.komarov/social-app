package net.styleru.ikomarov.data_layer.entities.search;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.search.SocialNetworkLinkEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkLinkEntity {

    @NonNull
    @SerializedName(FIELD_NETWORK_NAME)
    private final String name;

    @NonNull
    @SerializedName(FIELD_MESSAGE_LINK)
    private final String link;

    private SocialNetworkLinkEntity(@NonNull String name, @NonNull String link) {
        this.name = name;
        this.link = link;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getLink() {
        return link;
    }
}
