package net.styleru.ikomarov.data_layer.entities.session;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.session.SessionRequestEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SessionRequestEntity {

    @NonNull
    @SerializedName(FIELD_USERNAME)
    private final String username;

    @NonNull
    @SerializedName(FIELD_PASSWORD)
    private final String password;

    private SessionRequestEntity(@NonNull String username, @NonNull String password) {
        this.username = username;
        this.password = password;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    public static final class Builder {

        @NonNull
        private final String username;

        @NonNull
        private final String password;

        public Builder(@NonNull String username, @NonNull String password) {
            this.username = username;
            this.password = password;
        }

        @NonNull
        public SessionRequestEntity create() {
            return new SessionRequestEntity(username, password);
        }
    }
}
