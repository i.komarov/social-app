package net.styleru.ikomarov.data_layer.entities.user;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.user.UserRequestEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserRequestEntity {

    @NonNull
    @SerializedName(FIELD_USERNAME)
    private final String username;

    @NonNull
    @SerializedName(FIELD_PASSWORD)
    private final String password;

    @NonNull
    @SerializedName(FIELD_EMAIL)
    private final String email;

    private UserRequestEntity(@NonNull String username, @NonNull String password, @NonNull String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public static final class Builder {

        @NonNull
        private final String username;

        @NonNull
        private final String password;

        @NonNull
        private final String email;

        public Builder(@NonNull String username, @NonNull String password, @NonNull String email) {
            this.username = username;
            this.password = password;
            this.email = email;
        }

        public UserRequestEntity create() {
            return new UserRequestEntity(username, password, email);
        }
    }
}
