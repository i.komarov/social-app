package net.styleru.ikomarov.data_layer.services.session;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.common.Endpoints;
import net.styleru.ikomarov.data_layer.entities.session.SessionRequestEntity;
import net.styleru.ikomarov.data_layer.entities.session.SessionTokenEntity;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface ISessionRemoteService {

    /**
     * successful request is indicated with {@link java.net.HttpURLConnection#HTTP_CREATED}
     * */
    @POST(Endpoints.ENDPOINT_AUTHENTICATE)
    Call<SessionTokenEntity> authenticate(
            @Body SessionRequestEntity body
    );
}
