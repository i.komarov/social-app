package net.styleru.ikomarov.data_layer.entities.social;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.SerializedName;

import static net.styleru.ikomarov.data_layer.mapping.social.SocialNetworkEntityMapping.*;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkEntity {

    @NonNull
    @SerializedName(FIELD_NETWORK_NAME)
    private final String name;

    @NonNull
    @SerializedName(FIELD_USER_ID)
    private final String userId;

    @Nullable
    @SerializedName(FIELD_TOKEN)
    private final String token;

    private SocialNetworkEntity(@NonNull String name, @NonNull String userId, @Nullable String token) {
        this.name = name;
        this.userId = userId;
        this.token = token;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }

    @Nullable
    public String getToken() {
        return token;
    }

    public static final class Builder {

        @NonNull
        private final String name;

        @NonNull
        private final String userId;

        @Nullable
        private String token;

        public Builder(@NonNull String name, @NonNull String userId) {
            this.name = name;
            this.userId = userId;
        }

        public Builder withToken(@Nullable String token) {
            this.token = token;
            return this;
        }

        @NonNull
        public SocialNetworkEntity create() {
            return new SocialNetworkEntity(name, userId, token);
        }
    }
}
