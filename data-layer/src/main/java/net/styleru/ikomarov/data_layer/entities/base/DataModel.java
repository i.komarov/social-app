package net.styleru.ikomarov.data_layer.entities.base;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.mapping.base.IRemoteMapping;

import java.util.List;

/**
 * Created by i_komarov on 22.04.17.
 */

public abstract class DataModel<T> {

    public abstract boolean containsError();

    public abstract boolean containsData();

    public abstract List<T> getBody(IRemoteMapping<T> mapping);

    public abstract ExceptionBundle getError();
}
