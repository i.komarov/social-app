package net.styleru.ikomarov.domain_layer.mapping.direct.social;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class DirectSocialNetworksMapper implements Function<List<SocialNetworkEntity>, List<SocialNetworkDTO>> {

    @NonNull
    private final DirectSocialNetworkMapper mapper;

    public DirectSocialNetworksMapper(@NonNull DirectSocialNetworkMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<SocialNetworkDTO> apply(List<SocialNetworkEntity> socialNetworkEntities) throws Exception {
        return Observable.fromIterable(socialNetworkEntities)
                .map(mapper)
                .reduce(new ArrayList<SocialNetworkDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
