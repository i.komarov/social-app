package net.styleru.ikomarov.domain_layer.use_cases.social;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;
import net.styleru.ikomarov.domain_layer.contracts.operation.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;
import net.styleru.ikomarov.domain_layer.repository.social.ISocialNetworksRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkAssociateUseCase extends UseCase {

    @NonNull
    private final ISocialNetworksRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public SocialNetworkAssociateUseCase(@NonNull ISocialNetworksRepository repository,
                                    @NonNull Scheduler executeScheduler,
                                    @NonNull Scheduler postScheduler) {
        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(Callbacks callbacks, SocialNetworkDTO socialNetwork) {
        repository.associate(socialNetwork)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createSocialNetworksListObserver(callbacks));
    }

    private DisposableObserver<OperationStatus> createSocialNetworksListObserver(Callbacks callbacks) {
        DisposableObserver<OperationStatus> socialNetworkAssociatingEventObserver = new DisposableObserver<OperationStatus>() {
            @Override
            public void onNext(OperationStatus value) {
                if(!isDisposed()) {
                    if(value == OperationStatus.SUCCESS) {
                        callbacks.onSocialNetworkAssociated();
                    } else {
                        callbacks.onSocialNetworkAssociationFailed(new ExceptionBundle(Reason.INTERNAL_DOMAIN));
                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onSocialNetworkAssociationFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(socialNetworkAssociatingEventObserver);

        return socialNetworkAssociatingEventObserver;
    }

    public interface Callbacks {

        void onSocialNetworkAssociated();

        void onSocialNetworkAssociationFailed(ExceptionBundle error);
    }
}
