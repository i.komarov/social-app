package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.domain_layer.contracts.errors.IErrorTranslator;

/**
 * Created by i_komarov on 22.04.17.
 */

public class ErrorTranslationModule {

    private final Object lock = new Object();

    @Nullable
    private volatile IErrorTranslator oneNoteErrorCodeTranslator;

    public ErrorTranslationModule() {

    }

    @NonNull
    public IErrorTranslator provideOneNoteErrorCodeTranslator() {
        IErrorTranslator localInstance = oneNoteErrorCodeTranslator;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = oneNoteErrorCodeTranslator;
                if(localInstance == null) {
                    //TODO: add implementation for the error code translator
                    //localInstance = oneNoteErrorCodeTranslator = new OneNoteErrorCodeTranslator();
                }
            }
        }

        return localInstance;
    }
}
