package net.styleru.ikomarov.domain_layer.repository.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface ISearchRepository extends IRepository {

    Observable<List<SearchResultDTO>> search(@NonNull String query);
}
