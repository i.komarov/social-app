package net.styleru.ikomarov.domain_layer.repository.base;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.contracts.operation.OperationStatus;

import java.util.UUID;

/**
 * Created by i_komarov on 22.04.17.
 */

public class OperationResult<T> {

    @NonNull
    private final UUID id;

    @NonNull
    private final OperationStatus status;

    @Nullable
    private final T data;

    @Nullable
    private final ExceptionBundle error;

    public static <T> OperationResult<T> dataResponse(UUID id, OperationStatus status, T data) {
        return new OperationResult<T>(id, status, data, null);
    }

    public static <T> OperationResult<T> errorResponse(UUID id, OperationStatus status, ExceptionBundle error) {
        return new OperationResult<T>(id, status, null, error);
    }

    private OperationResult(@NonNull UUID id, @NonNull OperationStatus status, @Nullable T data, @Nullable ExceptionBundle error) {
        this.id = id;
        this.status = status;
        this.data = data;
        this.error = error;
    }

    @NonNull
    public UUID getId() {
        return id;
    }

    @NonNull
    public OperationStatus getStatus() {
        return status;
    }

    @Nullable
    public T getData() {
        return data;
    }

    @Nullable
    public ExceptionBundle getError() {
        return error;
    }
}
