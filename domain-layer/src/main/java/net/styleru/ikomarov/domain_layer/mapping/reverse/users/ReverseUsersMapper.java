package net.styleru.ikomarov.domain_layer.mapping.reverse.users;

import net.styleru.ikomarov.data_layer.entities.user.UserRequestEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class ReverseUsersMapper implements Function<UserDTO, UserRequestEntity> {
    @Override
    public UserRequestEntity apply(UserDTO dto) throws Exception {
        return new UserRequestEntity.Builder(
                dto.getUsername(),
                dto.getPassword(),
                dto.getEmail()
        ).create();
    }
}
