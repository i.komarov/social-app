package net.styleru.ikomarov.domain_layer.use_cases.social;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;
import net.styleru.ikomarov.domain_layer.repository.search.ISearchRepository;
import net.styleru.ikomarov.domain_layer.repository.social.ISocialNetworksRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;
import net.styleru.ikomarov.domain_layer.use_cases.search.SearchUseCase;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksGetUseCase extends UseCase {

    @NonNull
    private final ISocialNetworksRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public SocialNetworksGetUseCase(@NonNull ISocialNetworksRepository repository,
                         @NonNull Scheduler executeScheduler,
                         @NonNull Scheduler postScheduler) {
        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Callbacks<T> callbacks, Function<List<SocialNetworkDTO>, List<T>> mapper) {
        repository.list()
                .map(mapper)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createSocialNetworksListObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createSocialNetworksListObserver(Callbacks<T> callbacks) {
        DisposableObserver<List<T>> socialNetworksListObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                if(!isDisposed()) {
                    callbacks.onSocialNetworksLoaded(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onSocialNetworksLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(socialNetworksListObserver);

        return socialNetworksListObserver;
    }

    public interface Callbacks<T> {

        void onSocialNetworksLoaded(List<T> results);

        void onSocialNetworksLoadingFailed(ExceptionBundle error);
    }
}
