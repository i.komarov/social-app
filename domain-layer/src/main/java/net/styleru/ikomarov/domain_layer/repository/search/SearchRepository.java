package net.styleru.ikomarov.domain_layer.repository.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.search.SearchResponse;
import net.styleru.ikomarov.data_layer.services.search.ISearchRemoteService;
import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;
import net.styleru.ikomarov.domain_layer.mapping.direct.search.DirectSearchResultMapper;
import net.styleru.ikomarov.domain_layer.mapping.direct.search.DirectSearchResultsMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchRepository extends AbstractRepository implements ISearchRepository {

    @NonNull
    private final ISearchRemoteService remoteService;

    @NonNull
    private final DirectSearchResultMapper singleMapper;

    @NonNull
    private final DirectSearchResultsMapper listMapper;

    private SearchRepository(@NonNull ISearchRemoteService remoteService) {
        super();
        this.remoteService = remoteService;
        this.singleMapper = new DirectSearchResultMapper();
        this.listMapper = new DirectSearchResultsMapper(singleMapper);
    }

    @Override
    public Observable<List<SearchResultDTO>> search(@NonNull String query) {
        return remoteService.search(query)
                .filter(Response::isSuccessful)
                .map(Response::body)
                .map(SearchResponse::getResults)
                .map(listMapper);
    }

    public static final class Factory {

        @NonNull
        private final ISearchRemoteService remoteService;

        public Factory(@NonNull ISearchRemoteService remoteService) {
            this.remoteService = remoteService;
        }

        public ISearchRepository create() {
            return new SearchRepository(remoteService);
        }
    }
}
