package net.styleru.ikomarov.domain_layer.use_cases.base;

import net.styleru.ikomarov.data_layer.contracts.errors.InternalErrorContract;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * Created by i_komarov on 22.04.17.
 */

public class UseCase {

    private List<Disposable> disposables;

    public UseCase() {
        this.disposables = new ArrayList<>();
    }

    protected final void addDisposable(Disposable disposable) {
        this.disposables.add(disposable);
    }

    public final void dispose() {
        for(Disposable disposable : disposables) {
            if(!disposable.isDisposed()) {
                disposable.dispose();
            }
        }

        disposables.clear();
    }

    protected static ExceptionBundle newUseCaseError(Throwable e) {
        ExceptionBundle error = new ExceptionBundle(Reason.INTERNAL_DOMAIN);
        error.addThrowableExtra(InternalErrorContract.KEY_THROWABLE, e);
        error.addStringExtra(InternalErrorContract.KEY_MESSAGE, e.getMessage());
        return error;
    }
}

