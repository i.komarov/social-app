package net.styleru.ikomarov.domain_layer.repository.base;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import net.styleru.ikomarov.data_layer.contracts.errors.HttpErrorContract;
import net.styleru.ikomarov.data_layer.entities.base.DataResponse;
import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;

import java.io.IOException;
import java.nio.charset.Charset;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.functions.Function;
import okhttp3.ResponseBody;
import okio.Buffer;
import okio.BufferedSource;
import retrofit2.Response;

import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

/**
 * Created by i_komarov on 22.04.17.
 */

public abstract class AbstractRepository {

    private Gson gson;

    protected AbstractRepository() {
        this.gson = new Gson();
    }

    protected final <T> Function<Response<DataResponse<T>>, ObservableSource<DataResponse<T>>> verifyResponse(Class<T> clazz, final Integer... successCodes) {
        return response -> {
            boolean fitsConditions = false;

            for(int i = 0; i < successCodes.length; i++) {
                if(response.code() == successCodes[i]) {
                    fitsConditions = true;
                    break;
                }
            }

            if(fitsConditions && response.body() != null) {
                return Observable.just(response.body());
            } else if(fitsConditions) {
                return Observable.just(DataResponse.<T>emptyRequestResult());
            } else if(response.body() != null && response.body().containsError()) {
                return Observable.error(response.body().getError());
            } else {
                try {
                    ExceptionBundle oneNoteError;
                    if(null != (oneNoteError = this.<T>tryParseErrorResponse(parseResponseBody(response.errorBody())))) {
                        return Observable.error(oneNoteError);
                    }
                } catch (Exception e) {

                }

                if(response.code() == HTTP_UNAUTHORIZED) {
                    return Observable.error(new ExceptionBundle(Reason.SESSION_EXPIRED));
                } else {
                    ExceptionBundle error = new ExceptionBundle(Reason.HTTP_BAD_CODE);
                    error.addIntExtra(HttpErrorContract.KEY_CODE, response.code());
                    error.addStringExtra(HttpErrorContract.KEY_MESSAGE, response.message());
                    return Observable.error(error);
                }
            }
        };
    }

    private <T> ExceptionBundle tryParseErrorResponse(String responseJson) throws IOException {
        DataResponse<T> response = gson.fromJson(responseJson, new TypeToken<DataResponse<T>>(){}.getType());
        return response.getError();
    }

    private String parseResponseBody(ResponseBody responseBody) throws IOException {
        BufferedSource source = responseBody.source();
        source.request(Long.MAX_VALUE);
        Buffer buffer = source.buffer();

        Charset charset = Charset.forName("UTF-8");
        return buffer.readString(charset);
    }
}
