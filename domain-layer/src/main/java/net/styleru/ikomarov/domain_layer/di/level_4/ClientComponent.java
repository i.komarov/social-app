package net.styleru.ikomarov.domain_layer.di.level_4;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.ISessionComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public class ClientComponent implements IClientComponent {

    @NonNull
    private final ClientModule client;

    public ClientComponent(@NonNull ISessionComponent parent) {
        this.client = new ClientModule(parent);
    }

    @NonNull
    @Override
    public ClientModule clients() {
        return client;
    }
}
