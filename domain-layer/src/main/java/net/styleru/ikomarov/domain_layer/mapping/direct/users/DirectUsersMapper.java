package net.styleru.ikomarov.domain_layer.mapping.direct.users;

import net.styleru.ikomarov.data_layer.entities.user.UserEntity;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class DirectUsersMapper implements Function<UserEntity, UserDTO> {

    @Override
    public UserDTO apply(UserEntity entity) throws Exception {
        return new UserDTO(
                entity.getUsername(),
                entity.getPassword(),
                entity.getEmail()
        );
    }
}
