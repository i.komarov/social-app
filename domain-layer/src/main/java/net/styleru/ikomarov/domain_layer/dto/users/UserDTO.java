package net.styleru.ikomarov.domain_layer.dto.users;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserDTO {

    @NonNull
    private final String username;

    @NonNull
    private final String password;

    @NonNull
    private final String email;

    public UserDTO(@NonNull String username, @NonNull String password, @NonNull String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    @NonNull
    public String getUsername() {
        return username;
    }

    @NonNull
    public String getPassword() {
        return password;
    }

    @NonNull
    public String getEmail() {
        return email;
    }
}
