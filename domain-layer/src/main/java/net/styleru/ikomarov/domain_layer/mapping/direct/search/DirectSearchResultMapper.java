package net.styleru.ikomarov.domain_layer.mapping.direct.search;

import net.styleru.ikomarov.data_layer.entities.search.SearchResultEntity;
import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;
import net.styleru.ikomarov.domain_layer.dto.search.SocialNetworkLinkDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class DirectSearchResultMapper implements Function<SearchResultEntity, SearchResultDTO> {

    @Override
    public SearchResultDTO apply(SearchResultEntity entity) throws Exception {
        List<SocialNetworkLinkDTO> accumulator = new ArrayList<>();
        return new SearchResultDTO(
                entity.getId(),
                entity.getName(),
                entity.getPhotoUrl(),
                Observable.fromIterable(entity.getLinks()).map(link -> new SocialNetworkLinkDTO(link.getName(), link.getLink())).reduce(accumulator, (list, element) -> {
                    list.add(element);
                    return list;
                }).blockingGet(),
                entity.getCriteria(),
                entity.getExtendedCriteria(),
                entity.getMatchScore(),
                entity.getRelation()

        );
    }
}
