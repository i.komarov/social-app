package net.styleru.ikomarov.domain_layer.dto.search;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkLinkDTO {

    @NonNull
    private final String name;

    @NonNull
    private final String messageLink;

    public SocialNetworkLinkDTO(@NonNull String name, @NonNull String messageLink) {
        this.name = name;
        this.messageLink = messageLink;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getMessageLink() {
        return messageLink;
    }
}
