package net.styleru.ikomarov.domain_layer.use_cases.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;
import net.styleru.ikomarov.domain_layer.repository.search.ISearchRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;

import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchUseCase extends UseCase {

    @NonNull
    private final ISearchRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public SearchUseCase(@NonNull ISearchRepository repository,
                         @NonNull Scheduler executeScheduler,
                         @NonNull Scheduler postScheduler) {
        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public <T> void execute(Callbacks<T> callbacks, Function<List<SearchResultDTO>, List<T>> mapper, String query) {
        repository.search(query)
                .map(mapper)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createSearchResultsObserver(callbacks));
    }

    private <T> DisposableObserver<List<T>> createSearchResultsObserver(Callbacks<T> callbacks) {
        DisposableObserver<List<T>> searchResultsObserver = new DisposableObserver<List<T>>() {
            @Override
            public void onNext(List<T> value) {
                if(!isDisposed()) {
                    callbacks.onSearchResultsLoaded(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onSearchResultsLoadingFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(searchResultsObserver);

        return searchResultsObserver;
    }

    public interface Callbacks<T> {

        void onSearchResultsLoaded(List<T> results);

        void onSearchResultsLoadingFailed(ExceptionBundle error);
    }
}
