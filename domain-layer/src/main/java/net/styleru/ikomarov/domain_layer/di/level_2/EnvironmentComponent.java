package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.ISessionComponent;
import net.styleru.ikomarov.domain_layer.di.level_3.SessionComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public class EnvironmentComponent implements IEnvironmentComponent {
    @NonNull
    private final IAppComponent appComponent;

    @NonNull
    private final NetworkModule network;

    @NonNull
    private final PreferencesModule preferences;

    @NonNull
    private final ErrorTranslationModule errorTranslation;

    public EnvironmentComponent(@NonNull IAppComponent parent) {
        this.appComponent = parent;
        this.network = new NetworkModule(parent);
        this.preferences = new PreferencesModule(parent);
        this.errorTranslation = new ErrorTranslationModule();
    }

    @NonNull
    @Override
    public ISessionComponent plusSessionComponent() {
        return new SessionComponent(appComponent, this);
    }

    @NonNull
    @Override
    public NetworkModule network() {
        return network;
    }

    @NonNull
    @Override
    public PreferencesModule preferences() {
        return preferences;
    }

    @NonNull
    @Override
    public ErrorTranslationModule errorTranslation() {
        return errorTranslation;
    }
}
