package net.styleru.ikomarov.domain_layer.di.level_3;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.ClientComponent;
import net.styleru.ikomarov.domain_layer.di.level_4.IClientComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public class SessionComponent implements ISessionComponent {

    @NonNull
    private final SessionModule session;

    public SessionComponent(@NonNull IAppComponent appComponent, @NonNull IEnvironmentComponent parent) {
        this.session = new SessionModule(parent);
    }

    @NonNull
    @Override
    public IClientComponent plusClientComponent() {
        return new ClientComponent(this);
    }

    @NonNull
    @Override
    public SessionModule session() {
        return session;
    }
}
