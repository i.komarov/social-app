package net.styleru.ikomarov.domain_layer.use_cases.user;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.exceptions.ExceptionBundle;
import net.styleru.ikomarov.data_layer.exceptions.Reason;
import net.styleru.ikomarov.domain_layer.contracts.operation.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.repository.users.IUsersRepository;
import net.styleru.ikomarov.domain_layer.use_cases.base.UseCase;
import net.styleru.ikomarov.domain_layer.use_cases.social.SocialNetworkAssociateUseCase;

import io.reactivex.Scheduler;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UserRegisterUseCase extends UseCase {

    @NonNull
    private final IUsersRepository repository;

    @NonNull
    private final Scheduler executeScheduler;

    @NonNull
    private final Scheduler postScheduler;

    public UserRegisterUseCase(@NonNull IUsersRepository repository,
                                         @NonNull Scheduler executeScheduler,
                                         @NonNull Scheduler postScheduler) {
        super();
        this.repository = repository;
        this.executeScheduler = executeScheduler;
        this.postScheduler = postScheduler;
    }

    public void execute(Callbacks callbacks, UserDTO user) {
        repository.register(user)
                .subscribeOn(executeScheduler)
                .observeOn(postScheduler)
                .subscribe(createUserRegisterEventObserver(callbacks));
    }

    private DisposableObserver<UserDTO> createUserRegisterEventObserver(Callbacks callbacks) {
        DisposableObserver<UserDTO> userRegisteredEventObserver = new DisposableObserver<UserDTO>() {
            @Override
            public void onNext(UserDTO value) {
                if(!isDisposed()) {
                    callbacks.onUserRegistered(value);
                }
            }

            @Override
            public void onError(Throwable e) {
                if(!isDisposed()) {
                    callbacks.onUserRegistrationFailed(e instanceof ExceptionBundle ? (ExceptionBundle) e : newUseCaseError(e));
                }
            }

            @Override
            public void onComplete() {

            }
        };

        addDisposable(userRegisteredEventObserver);

        return userRegisteredEventObserver;
    }

    public interface Callbacks {

        void onUserRegistered(UserDTO user);

        void onUserRegistrationFailed(ExceptionBundle error);
    }
}
