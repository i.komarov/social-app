package net.styleru.ikomarov.domain_layer.mapping.direct.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.search.SearchResultEntity;
import net.styleru.ikomarov.domain_layer.dto.search.SearchResultDTO;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class DirectSearchResultsMapper implements Function<List<SearchResultEntity>, List<SearchResultDTO>> {

    @NonNull
    private final DirectSearchResultMapper mapper;

    public DirectSearchResultsMapper(@NonNull DirectSearchResultMapper mapper) {
        this.mapper = mapper;
    }

    @Override
    public List<SearchResultDTO> apply(List<SearchResultEntity> searchResultEntities) throws Exception {
        return Observable.fromIterable(searchResultEntities)
                .map(mapper)
                .reduce(new ArrayList<SearchResultDTO>(), (list, element) -> {
                    list.add(element);
                    return list;
                })
                .blockingGet();
    }
}
