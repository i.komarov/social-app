package net.styleru.ikomarov.domain_layer.di.level_1;

import android.content.Context;
import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_2.EnvironmentComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public class AppComponent implements IAppComponent {

    @NonNull
    private final AppModule appModule;

    private AppComponent(@NonNull Context context) {
        this.appModule = new AppModule(context);
    }

    public static AppComponent fromAppProcess(@NonNull Context context) {
        return new AppComponent(context.getApplicationContext());
    }

    public static AppComponent fromExternalProcess(@NonNull Context context) {
        AppComponent component = new AppComponent(context);
        return component;
    }

    @NonNull
    @Override
    public AppModule app() {
        return this.appModule;
    }

    @NonNull
    @Override
    public IEnvironmentComponent plusEnvironmentComponent() {
        return new EnvironmentComponent(this);
    }
}
