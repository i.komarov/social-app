package net.styleru.ikomarov.domain_layer.dto.search;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;

import java.util.List;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SearchResultDTO {

    @NonNull
    private final String id;

    @NonNull
    private final String name;

    @NonNull
    private final String photoUrl;

    @NonNull
    private final List<SocialNetworkLinkDTO> socialNetworks;

    @NonNull
    private final String criteria;

    @NonNull
    private final String extendedCriteria;

    @NonNull
    private final String matchScore;

    @NonNull
    private final String relation;

    public SearchResultDTO(@NonNull String id,
                           @NonNull String name,
                           @NonNull String photoUrl,
                           @NonNull List<SocialNetworkLinkDTO> socialNetworks,
                           @NonNull String criteria,
                           @NonNull String extendedCriteria,
                           @NonNull String matchScore,
                           @NonNull String relation) {

        this.id = id;
        this.name = name;
        this.photoUrl = photoUrl;
        this.socialNetworks = socialNetworks;
        this.criteria = criteria;
        this.extendedCriteria = extendedCriteria;
        this.matchScore = matchScore;
        this.relation = relation;
    }

    @NonNull
    public String getId() {
        return id;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getPhotoUrl() {
        return photoUrl;
    }

    @NonNull
    public List<SocialNetworkLinkDTO> getSocialNetworks() {
        return socialNetworks;
    }

    @NonNull
    public String getCriteria() {
        return criteria;
    }

    @NonNull
    public String getExtendedCriteria() {
        return extendedCriteria;
    }

    @NonNull
    public String getMatchScore() {
        return matchScore;
    }

    @NonNull
    public String getRelation() {
        return relation;
    }
}
