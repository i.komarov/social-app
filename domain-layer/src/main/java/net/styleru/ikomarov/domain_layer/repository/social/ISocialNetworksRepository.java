package net.styleru.ikomarov.domain_layer.repository.social;

import net.styleru.ikomarov.domain_layer.contracts.operation.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface ISocialNetworksRepository {

    Observable<List<SocialNetworkDTO>> list();

    Observable<OperationStatus> associate(SocialNetworkDTO network);
}
