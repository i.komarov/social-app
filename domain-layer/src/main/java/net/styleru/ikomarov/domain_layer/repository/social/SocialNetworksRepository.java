package net.styleru.ikomarov.domain_layer.repository.social;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.services.social.ISocialNetworksRemoteService;
import net.styleru.ikomarov.domain_layer.contracts.operation.OperationStatus;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;
import net.styleru.ikomarov.domain_layer.mapping.direct.social.DirectSocialNetworkMapper;
import net.styleru.ikomarov.domain_layer.mapping.direct.social.DirectSocialNetworksMapper;
import net.styleru.ikomarov.domain_layer.mapping.reverse.social.ReverseSocialNetworkMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;

import java.net.HttpURLConnection;
import java.util.List;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworksRepository extends AbstractRepository implements ISocialNetworksRepository {

    @NonNull
    private final ISocialNetworksRemoteService remoteService;

    @NonNull
    private final DirectSocialNetworkMapper singleMapper;

    @NonNull
    private final DirectSocialNetworksMapper listMapper;

    @NonNull
    private final ReverseSocialNetworkMapper reverseSingleMapper;

    private SocialNetworksRepository(@NonNull ISocialNetworksRemoteService remoteService) {
        super();
        this.remoteService = remoteService;
        this.singleMapper = new DirectSocialNetworkMapper();
        this.listMapper = new DirectSocialNetworksMapper(singleMapper);
        this.reverseSingleMapper = new ReverseSocialNetworkMapper();
    }

    @Override
    public Observable<List<SocialNetworkDTO>> list() {
        return remoteService.list()
                .filter(Response::isSuccessful)
                .map(Response::body)
                .map(listMapper);
    }

    @Override
    public Observable<OperationStatus> associate(SocialNetworkDTO network) {
        return Observable.just(network)
                .map(reverseSingleMapper)
                .flatMap(remoteService::associate)
                .map(response -> response.code() == HttpURLConnection.HTTP_CREATED ?
                        OperationStatus.SUCCESS :
                        OperationStatus.FAILURE
                );
    }

    public static final class Factory {

        @NonNull
        private final ISocialNetworksRemoteService remoteService;

        public Factory(@NonNull ISocialNetworksRemoteService remoteService) {
            this.remoteService = remoteService;
        }

        public ISocialNetworksRepository create() {
            return new SocialNetworksRepository(remoteService);
        }
    }
}
