package net.styleru.ikomarov.domain_layer.contracts.operation;

/**
 * Created by i_komarov on 22.04.17.
 */

public enum OperationStatus {
    SUCCESS(00000000),
    FAILURE(00000001),
    DELAYED(00000002);

    private final int code;

    OperationStatus(int code) {
        this.code = code;
    }

    public int code() {
        return this.code;
    }
}
