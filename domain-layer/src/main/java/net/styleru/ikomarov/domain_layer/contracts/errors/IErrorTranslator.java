package net.styleru.ikomarov.domain_layer.contracts.errors;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface IErrorTranslator {

    @NonNull
    String translate(int errorCode);
}
