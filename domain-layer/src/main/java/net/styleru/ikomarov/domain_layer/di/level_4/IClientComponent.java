package net.styleru.ikomarov.domain_layer.di.level_4;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface IClientComponent {

    @NonNull
    ClientModule clients();
}
