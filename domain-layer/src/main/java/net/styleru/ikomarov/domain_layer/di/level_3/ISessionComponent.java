package net.styleru.ikomarov.domain_layer.di.level_3;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_4.IClientComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface ISessionComponent {

    @NonNull
    IClientComponent plusClientComponent();

    @NonNull
    SessionModule session();
}
