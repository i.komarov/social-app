package net.styleru.ikomarov.domain_layer.mapping.direct.social;

import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class DirectSocialNetworkMapper implements Function<SocialNetworkEntity, SocialNetworkDTO> {

    @Override
    public SocialNetworkDTO apply(SocialNetworkEntity entity) throws Exception {
        return new SocialNetworkDTO(
                entity.getName(),
                entity.getUserId()
        );
    }
}
