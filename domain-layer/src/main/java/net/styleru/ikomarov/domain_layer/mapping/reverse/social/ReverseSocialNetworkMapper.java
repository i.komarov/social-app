package net.styleru.ikomarov.domain_layer.mapping.reverse.social;

import net.styleru.ikomarov.data_layer.entities.social.SocialNetworkEntity;
import net.styleru.ikomarov.domain_layer.dto.social.SocialNetworkDTO;

import io.reactivex.functions.Function;

/**
 * Created by i_komarov on 23.04.17.
 */

public class ReverseSocialNetworkMapper implements Function<SocialNetworkDTO, SocialNetworkEntity> {

    @Override
    public SocialNetworkEntity apply(SocialNetworkDTO dto) throws Exception {
        return new SocialNetworkEntity.Builder(
                dto.getName(),
                dto.getUserId()
        ).create();
    }
}
