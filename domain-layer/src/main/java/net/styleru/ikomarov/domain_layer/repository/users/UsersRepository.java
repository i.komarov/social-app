package net.styleru.ikomarov.domain_layer.repository.users;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.data_layer.entities.user.UserRequestEntity;
import net.styleru.ikomarov.data_layer.services.users.IUsersRemoteService;
import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.mapping.direct.users.DirectUsersMapper;
import net.styleru.ikomarov.domain_layer.mapping.reverse.users.ReverseUsersMapper;
import net.styleru.ikomarov.domain_layer.repository.base.AbstractRepository;

import io.reactivex.Observable;
import retrofit2.Response;

/**
 * Created by i_komarov on 23.04.17.
 */

public class UsersRepository extends AbstractRepository implements IUsersRepository {

    @NonNull
    private final IUsersRemoteService remoteService;

    @NonNull
    private final DirectUsersMapper directMapper;

    @NonNull
    private final ReverseUsersMapper reverseMapper;

    private UsersRepository(@NonNull IUsersRemoteService remoteService) {
        super();
        this.remoteService = remoteService;
        this.directMapper = new DirectUsersMapper();
        this.reverseMapper = new ReverseUsersMapper();
    }

    @Override
    public Observable<UserDTO> register(UserDTO user) {
        return Observable.just(user)
                .map(reverseMapper)
                .flatMap(entity ->remoteService.register(new UserRequestEntity.Builder(user.getUsername(), user.getPassword(), user.getEmail()).create()))
                .filter(Response::isSuccessful)
                .map(Response::body)
                .map(directMapper);
    }

    public static final class Factory {

        @NonNull
        private final IUsersRemoteService remoteService;

        public Factory(@NonNull IUsersRemoteService remoteService) {
            this.remoteService = remoteService;
        }

        public IUsersRepository create() {
            return new UsersRepository(remoteService);
        }
    }
}
