package net.styleru.ikomarov.domain_layer.repository.base;

import android.support.annotation.NonNull;

import java.util.UUID;

/**
 * Created by i_komarov on 22.04.17.
 */

public class ResourceOperation {

    @NonNull
    private final UUID id;

    @NonNull
    private final String resource;

    public ResourceOperation(@NonNull UUID id, @NonNull String resource) {
        this.id = id;
        this.resource = resource;
    }

    @NonNull
    public UUID getId() {
        return id;
    }

    @NonNull
    public String getResource() {
        return resource;
    }
}
