package net.styleru.ikomarov.domain_layer.di.level_4;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.manager.session.interceptor.ClientSessionInterceptor;
import net.styleru.ikomarov.domain_layer.di.level_3.ISessionComponent;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by i_komarov on 22.04.17.
 */

public class ClientModule {

    private final long READ_TIMEOUT = 30L;

    private final long WRITE_TIMEOUT = 15L;

    private final long CONNECT_TIMEOUT = 15L;

    private final Object clientLock = new Object();

    @NonNull
    private final ISessionComponent component;

    @Nullable
    private volatile OkHttpClient oneNoteClient;

    public ClientModule(@NonNull ISessionComponent component) {
        this.component = component;
    }

    @NonNull
    public OkHttpClient provideClient() {
        OkHttpClient localInstance = oneNoteClient;
        if(localInstance == null) {
            synchronized (clientLock) {
                localInstance = oneNoteClient;
                if(localInstance == null) {
                    localInstance = oneNoteClient = new OkHttpClient.Builder()
                            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
                            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                            .addInterceptor(new ClientSessionInterceptor(component.session().provideSessionManager()))
                            .build();
                }
            }
        }

        return localInstance;
    }
}
