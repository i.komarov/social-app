package net.styleru.ikomarov.domain_layer.di.level_2;

import android.support.annotation.NonNull;

import net.styleru.ikomarov.domain_layer.di.level_3.ISessionComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public interface IEnvironmentComponent {

    @NonNull
    ISessionComponent plusSessionComponent();

    @NonNull
    NetworkModule network();

    @NonNull
    PreferencesModule preferences();

    @NonNull
    ErrorTranslationModule errorTranslation();
}
