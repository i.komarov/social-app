package net.styleru.ikomarov.domain_layer.repository.users;

import net.styleru.ikomarov.domain_layer.dto.users.UserDTO;
import net.styleru.ikomarov.domain_layer.repository.base.IRepository;

import io.reactivex.Observable;

/**
 * Created by i_komarov on 23.04.17.
 */

public interface IUsersRepository extends IRepository {

    Observable<UserDTO> register(UserDTO user);
}
