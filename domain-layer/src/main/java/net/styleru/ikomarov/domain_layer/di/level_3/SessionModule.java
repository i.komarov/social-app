package net.styleru.ikomarov.domain_layer.di.level_3;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import net.styleru.ikomarov.data_layer.manager.session.ISessionManager;
import net.styleru.ikomarov.data_layer.manager.session.SessionManager;
import net.styleru.ikomarov.domain_layer.di.level_1.IAppComponent;
import net.styleru.ikomarov.domain_layer.di.level_2.IEnvironmentComponent;

/**
 * Created by i_komarov on 22.04.17.
 */

public class SessionModule {

    private final Object lock = new Object();

    @NonNull
    private final IEnvironmentComponent component;

    @Nullable
    private volatile ISessionManager sessionManager;

    public SessionModule(@NonNull IEnvironmentComponent component) {
        this.component = component;
    }

    @NonNull
    public ISessionManager provideSessionManager() {
        ISessionManager localInstance = sessionManager;
        if(localInstance == null) {
            synchronized (lock) {
                localInstance = sessionManager;
                if(localInstance == null) {
                    localInstance = sessionManager = new SessionManager(component.preferences().provideSharedPreferences(), component.network().provideNetworkManager());
                }
            }
        }

        return localInstance;
    }
}

