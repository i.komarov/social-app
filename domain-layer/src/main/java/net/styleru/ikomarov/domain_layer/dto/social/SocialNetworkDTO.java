package net.styleru.ikomarov.domain_layer.dto.social;

import android.support.annotation.NonNull;

/**
 * Created by i_komarov on 23.04.17.
 */

public class SocialNetworkDTO {

    @NonNull
    private final String name;

    @NonNull
    private final String userId;

    public SocialNetworkDTO(@NonNull String name, @NonNull String userId) {
        this.name = name;
        this.userId = userId;
    }

    @NonNull
    public String getName() {
        return name;
    }

    @NonNull
    public String getUserId() {
        return userId;
    }
}
